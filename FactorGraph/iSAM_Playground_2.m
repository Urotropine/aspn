%% This is the all plot version of example fusion.
%% Header and Flags
NeedRereadFile = 1;
clc;
import gtsam.*;
disp('Fusion test on Px4 Using Factor Graph')
%% Raw data reading and pre-process section
% Read data from file
if NeedRereadFile
clear;
disp('-->Begin read data form file.');
    FileIMUAtmData = ReadRosData('IMUAtmData.txt');
    FileIMUDataData = ReadRosData('IMUDataData.txt');
    FileIMUDataRawData = ReadRosData('IMUDataRawData.txt');
    FileIMUMagData = ReadRosData('IMUMagData.txt');
    FileIMUTempData = ReadRosData('IMUTempData.txt');
    FileLocalPostionLocalData = ReadRosData('LocalPostionLocalData.txt');
    FileLocalPostionPoseData = ReadRosData('LocalPostionPoseData.txt');
    FileLocalPostionVelocityData = ReadRosData('LocalPostionVelocityData.txt');
    FilePositionGlobaGlobalData = ReadRosData('PositionGlobaGlobalData.txt');
    FilePositionGlobalCompassData = ReadRosData('PositionGlobalCompassData.txt');
    FilePositionGlobalGpVelData = ReadRosData('PositionGlobalGpVelData.txt');
    FilePositionGlobalLocalData = ReadRosData('PositionGlobalLocalData.txt');
    FilePositionGlobalRawFixData = ReadRosData('PositionGlobalRawFixData.txt');
    FilePositionGlobalRawGPSVelData = ReadRosData('PositionGlobalRawGPSVelData.txt');
    FilePositionGlobalRelAltData = ReadRosData('PositionGlobalRelAltData.txt');
    FilePx4flowOpticalFlowRadData = ReadRosData('Px4flowOpticalFlowRadData.txt');
disp('<--Reading data done.');
end
% Pre-process of data
disp('-->Processing raw structure.')
% IMU data
disp('----> IMU data')
    fn = fieldnames(FileIMUDataData);
    % IMUData Time
    t=num2cell(([FileIMUDataData.IMUDataTime]'-FileIMUDataData(1).IMUDataTime)*(1e-9));
    [IMUData(1:length(FileIMUDataData)).Time]=deal(t{:});
    % IMUData Dt
    p=[IMUData.Time];q=[0,p];
    t=num2cell(p'-q(1:length(q)-1)');
    [IMUData.Dt]=deal(t{:});
    % Frame Trans:original is x:forward,y:left,z:up.we need to change it to
    % x;left,y:forward,z:up to fit the GPS ENU.
        % This step is only done to Q acc and gyro for now with cov
        % unchanged! TODO:cov
        
    % IMU Orign
    t = cellfun(@(x) x', num2cell([[FileIMUDataData.IMUDataOrienQw]' -[FileIMUDataData.IMUDataOrienQy]'...
        [FileIMUDataData.IMUDataOrienQx]' [FileIMUDataData.IMUDataOrienQz]' ...
        ],2), 'UniformOutput', false); %NOTICE: FRAME TRANS HERE !!!!(x_n=-y_b y_n=x_b)
    [IMUData.Q]=deal(t{:});
     % IMU Orign Cov
    t=[FileIMUDataData.IMUDataOrienCovElems0]';
    for i=[8:15]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_Q]=deal(t{:});
    % IMU acc   
    t=num2cell([FileIMUDataData.IMUDataLinearAccX]');
    [IMUData.AccY]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(y=x)
    t=num2cell(-[FileIMUDataData.IMUDataLinearAccY]');
    [IMUData.AccX]=deal(t{:});  %NOTICE: FRAME TRANS HERE !!!!(x=-y)
    t=num2cell([FileIMUDataData.IMUDataLinearAccZ]');
    [IMUData.AccZ]=deal(t{:}); 
    %IMU acc Cov
    t=[FileIMUDataData.IMUDataLinearAccCovElems0]';
    for i=[32:39]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_acc]=deal(t{:});  
    % IMU Omega   
    t=num2cell([FileIMUDataData.IMUDataAngVelX]');
    [IMUData.OmegaY]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(y=x)
    t=num2cell(-[FileIMUDataData.IMUDataAngVelY]');
    [IMUData.OmegaX]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(x=-y)
    t=num2cell([FileIMUDataData.IMUDataAngVelZ]');
    [IMUData.OmegaZ]=deal(t{:}); 
    %IMU Omega Cov
    t=[FileIMUDataData.IMUDataAngVelCovElems0]';
    for i=[20:27]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_omega]=deal(t{:});      
    % IMU acc-omega (required by isam) %NOTICE: FRAME TRANS HERE !!!!
    t = cellfun(@(x) x', num2cell([...
        -[FileIMUDataData.IMUDataLinearAccY]' [FileIMUDataData.IMUDataLinearAccX]' [FileIMUDataData.IMUDataLinearAccZ]' ...
        -[FileIMUDataData.IMUDataAngVelY]' [FileIMUDataData.IMUDataAngVelX]' [FileIMUDataData.IMUDataAngVelZ]'...
        ],2), 'UniformOutput', false);
   [IMUData.acc_omega] = deal(t{:});
   clear t p q;
% GPS data Here is ENU
disp('----> GPS data')
    fn = fieldnames(FilePositionGlobalRawFixData);
    % GPSData Time
    t=num2cell(([FilePositionGlobalRawFixData.PositionGlobalRawFixTime]'-FilePositionGlobalRawFixData(1).PositionGlobalRawFixTime)*(1e-9));
    [GPSData(1:length(FilePositionGlobalRawFixData)).Time]=deal(t{:});
    % GPS location
    Lat = [FilePositionGlobalRawFixData.PositionGlobalRawFixLat];
    Lon = [FilePositionGlobalRawFixData.PositionGlobalRawFixLon]; 
    Alt = [FilePositionGlobalRawFixData.PositionGlobalRawFixAlt];
    AvrAlt=7.8;%(m)%local alt
    [rm,~,rl]=ellradii(mean(Lat));%get radii of earth
    GPSDataX=deg2rad(Lon).*(rl+AvrAlt);
    GPSDataY=deg2rad(Lat).*(rm+AvrAlt);
    GPSDataZ=Alt-AvrAlt;
    % Frame trnas
    
    % reset cood
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);
    t=num2cell(GPSDataX');
    [GPSData.LocalX]=deal(t{:}); 
    t=num2cell(GPSDataY');
    [GPSData.LocalY]=deal(t{:}); 
    t=num2cell(GPSDataZ');
    [GPSData.LocalZ]=deal(t{:}); 
    % GPS position Cov
    t=[FilePositionGlobalRawFixData.PositionGlobalRawFixPositionCovElems0]';
    for i=[8:15]
        t= [t [FilePositionGlobalRawFixData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [GPSData.Cov_pose]=deal(t{:}); 
    % GPS postion combined used by iSAM
    for i = 1:numel(GPSData)
        GPSData(i).Position = gtsam.Point3(GPSData(i).LocalX, GPSData(i).LocalY, GPSData(i).LocalZ);
    end
       clear fn p q Lat Lon Alt AvrAlt i rl rm t;
% Optialflow data
disp('----> Opticalflow data')
    fn = fieldnames(FilePx4flowOpticalFlowRadData);
    % Optflow Time
    t=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadTime]'-FilePx4flowOpticalFlowRadData(1).Px4flowOpticalFlowRadTime)*(1e-9));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).Time]=deal(t{:});
    % Optflow int time
    t=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntTime_us]')*(1e-6));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).IntTime]=deal(t{:});
    % Optflow pose
    dy = [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntX];%NOTICE: FRAME TRANS HERE !!!!
    dx = [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntY];%NOTICE: FRAME TRANS HERE !!!!
    t_imu = [IMUData.Time];
    q_imu = [IMUData.Q];
    t=[OptflowData.Time];
    q=zeros(4,length(OptflowData));
    j=1;
    for i=1:length(t_imu)
        if j<=length(OptflowData)
            if t(j)==t_imu(i)
                q(:,j)=q_imu(:,i);
                j=j+1;continue;
            elseif t(j)<t_imu(min(i+1,length(t_imu)))
                q(:,j)=(q_imu(:,i)+q_imu(:,i+1))/2;
                j=j+1;
                continue;
            else
                continue;
            end
        end
    end
    yaw = atan2(2.*q(1,:).*q(4,:)+2.*q(2,:).*q(3,:),1-2.*q(3,:).*q(3,:)-2.*q(4,:).*q(4,:));
    dx_fix = cos(yaw) .* (-1.*dx) - sin(yaw) .* (-1.*dy);
    dy_fix = sin(yaw) .* (-1.*dx) + cos(yaw) .* (-1.*dy);

%     t=quatrotate(q',[dx' dy' zeros(length(dx),1)]);
%     dx_fix=t(:,1);
%     dy_fix=t(:,2);
    
    X=zeros(1,length(dx));X(1)=dx_fix(1);
    Y=X;Y(1)=dy_fix(1);
    for i=2:length(OptflowData)
        X(i)=X(i-1)+dx_fix(i-1);
        Y(i)=Y(i-1)+dy_fix(i-1);
    end
    Vx = X./t;Vy = Y./t;
    X=num2cell(X');Y=num2cell(Y');
    Vx=num2cell(Vx');Vy=num2cell(Vy');
    
    [OptflowData.X]=deal(X{:});
    [OptflowData.Y]=deal(Y{:}); 
    [OptflowData.Vx]=deal(Vx{:});
    [OptflowData.Vy]=deal(Vy{:}); 
 % matadata   
    disp('----> Reading sensor metadata')
    IMU_metadata = importdata('ExampleIMU_metadata');
    IMU_metadata = cell2struct(num2cell(IMU_metadata.data), IMU_metadata.colheaders, 2);
    IMUinBody = Pose3.Expmap([IMU_metadata.BodyPtx; IMU_metadata.BodyPty; IMU_metadata.BodyPtz;
    IMU_metadata.BodyPrx; IMU_metadata.BodyPry; IMU_metadata.BodyPrz; ]);
    if ~IMUinBody.equals(Pose3, 1e-5)
      error 'Currently only support IMUinBody is identity, i.e. IMU and body frame are the same';
    end   
 % Read data end
 disp('<-- Read data end')
 
 
 %% iSAM2 Begin here
 % ******************************************************
disp('-->Begin iSAM')

noiseModelGPS = noiseModel.Diagonal.Precisions([ [0;0;0]; 1/1 * [1;1;1] ]);
firstGPSPose = 1;
GPSskip = 3; % Skip this many GPS measurements each time

%% Get initial conditions for the estimated trajectory
currentPoseGlobal = Pose3(Rot3, GPSData(firstGPSPose).Position); % initial pose is the reference frame (navigation frame)
currentVelocityGlobal = LieVector([0;0;0]); % the vehicle is stationary at the beginning
currentBias = imuBias.ConstantBias(zeros(3,1), zeros(3,1));
sigma_init_x = noiseModel.Isotropic.Precisions([ 0.0; 0.0; 0.0; 1; 1; 1 ]);
sigma_init_v = noiseModel.Isotropic.Sigma(3, 1000.0);
sigma_init_b = noiseModel.Isotropic.Sigmas([ 0.100; 0.100; 0.100; 5.00e-05; 5.00e-05; 5.00e-05 ]);
sigma_between_b = [ IMU_metadata.AccelerometerBiasSigma * ones(3,1); IMU_metadata.GyroscopeBiasSigma * ones(3,1) ];
g = [0;0;-9.8];
w_coriolis = [0;0;0];

%% Solver object
isamParams = ISAM2Params;
isamParams.setFactorization('CHOLESKY');
isamParams.setRelinearizeSkip(10);
isam = gtsam.ISAM2(isamParams);
newFactors = NonlinearFactorGraph;
newValues = Values;

%% Main loop:
% (1) we read the measurements
% (2) we create the corresponding factors in the graph
% (3) we solve the graph to obtain and optimal estimate of robot trajectory
IMUtimes = [IMUData.Time];

disp('----> Starting main loop')

for measurementIndex = firstGPSPose:length(GPSData)
  fprintf('\n --------------------> \n Begin of the %d th GPS data.\n',measurementIndex);
  % At each non=IMU measurement we initialize a new node in the graph
  currentPoseKey = symbol('x',measurementIndex);
  currentVelKey =  symbol('v',measurementIndex);
  currentBiasKey = symbol('b',measurementIndex);
  t = GPSData(measurementIndex).Time;
  
  if measurementIndex == firstGPSPose
    %% Create initial estimate and prior on initial pose, velocity, and biases
    newValues.insert(currentPoseKey, currentPoseGlobal);
    newValues.insert(currentVelKey, currentVelocityGlobal);
    newValues.insert(currentBiasKey, currentBias);
    newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
    newFactors.add(PriorFactorLieVector(currentVelKey, currentVelocityGlobal, sigma_init_v));
    newFactors.add(PriorFactorConstantBias(currentBiasKey, currentBias, sigma_init_b));
    fprintf('#------> Initial factor and values caredted\n');
  else
    t_previous = GPSData(measurementIndex-1).Time;
    %% Summarize IMU data between the previous GPS measurement and now
    IMUindices = find(IMUtimes >= t_previous & IMUtimes <= t);
    
    currentSummarizedMeasurement = gtsam.ImuFactorPreintegratedMeasurements( ...
      currentBias, IMU_metadata.AccelerometerSigma.^2 * eye(3), ...
      IMU_metadata.GyroscopeSigma.^2 * eye(3), IMU_metadata.IntegrationSigma.^2 * eye(3));
    
    for imuIndex = IMUindices
      accMeas = [ IMUData(imuIndex).AccX; IMUData(imuIndex).AccY; IMUData(imuIndex).AccZ ];
      omegaMeas = [ IMUData(imuIndex).OmegaX; IMUData(imuIndex).OmegaY; IMUData(imuIndex).OmegaZ ];
      deltaT = IMUData(imuIndex).Dt;
      currentSummarizedMeasurement.integrateMeasurement(accMeas, omegaMeas, deltaT);
    end
    
    % Create IMU factor
    newFactors.add(ImuFactor( ...
      currentPoseKey-1, currentVelKey-1, ...
      currentPoseKey, currentVelKey, ...
      currentBiasKey, currentSummarizedMeasurement, g, w_coriolis));
    
    % Bias evolution as given in the IMU metadata
    newFactors.add(BetweenFactorConstantBias(currentBiasKey-1, currentBiasKey, imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
      noiseModel.Diagonal.Sigmas(sqrt(numel(IMUindices)) * sigma_between_b)));

    % Create GPS factor
    GPSPose = Pose3(currentPoseGlobal.rotation, GPSData(measurementIndex).Position);
    if mod(measurementIndex, GPSskip) == 0
      newFactors.add(PriorFactorPose3(currentPoseKey, GPSPose, noiseModelGPS));
    end

    % Add initial value
    newValues.insert(currentPoseKey, GPSPose);
    newValues.insert(currentVelKey, currentVelocityGlobal);
    newValues.insert(currentBiasKey, currentBias);
    
    %fprintf('------> ### At measurement GPS index %d \n',measurementIndex);
    % Update solver
    % =======================================================================
    % We accumulate 2*GPSskip GPS measurements before updating the solver at
    % first so that the heading becomes observable.
    if measurementIndex > firstGPSPose + 2*GPSskip
      fprintf('#------> Solver begin here.\n');
      isam.update(newFactors, newValues);
      newFactors = NonlinearFactorGraph;
      newValues = Values;
      
      if rem(measurementIndex,10)==0 % plot every 10 time steps
        cla;
        plot3DTrajectory(isam.calculateEstimate, 'r-');
        title('Estimated trajectory using ISAM2 (IMU+GPS)')
        xlabel('[m]')
        ylabel('[m]')
        zlabel('[m]')
        axis equal
        drawnow;
      end
      % =======================================================================

      currentPoseGlobal = isam.calculateEstimate(currentPoseKey);
      currentVelocityGlobal = isam.calculateEstimate(currentVelKey);
      currentBias = isam.calculateEstimate(currentBiasKey);
    end
  end
   fprintf('End of the %d th GPS data.\n ^^^^^^^^^^^^^^^^^^^^^^^^\n',measurementIndex);
end % end main loop



 