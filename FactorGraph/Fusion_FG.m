%% This is the all-plot version of GPS-IMU fusion
%% Log here:
% Dat with GPS and IMU 
% 160202_1 ############################################
% Ok now i suppose the reason for the ill pose is the too short integration
% of IMU. Then the counting on IMU integration will be done in this
% version. 
% ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
% 160202_2 ############################################
% Begin to feed the OPT Data 
% ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
% 160202_3 ############################################
% Ok here is the way to add opt factor:
% First we treat Optflow at the prior of the velocity.
%% Header and Flags
clear;clc;close all;
NeediSam2 = true;
import gtsam.*;
disp('Fusion test on Px4 Using Factor Graph')
%% Read Data
run('Fusion_ReadData.m')
%% Rebuild data bag
 disp('--> Building data bag for isam')
 TotalDataLength  = length(IMUData)+length(GPSData)+length(OptflowData);
 type = [];
 DataBag = cell(TotalDataLength,1); % create the data bag teh same length of all used sensors
 indexIMU=1;indexGPS=1;indexOPT=1;
 for i=1:TotalDataLength
     [~,MinType]=min([IMUData(indexIMU).Time,GPSData(indexGPS).Time,OptflowData(indexOPT).Time]);
     switch MinType
         case 1 % IMU
             DataBag{i} = IMUData(indexIMU);
             indexIMU = min(indexIMU+1,length(IMUData));
             type = [type DataBag{i}.Type];
         case 2 % GPS
             DataBag{i} = GPSData(indexGPS);
             indexGPS = min(indexGPS+1,length(GPSData));
             type = [type DataBag{i}.Type];
         case 3 % Opt
             DataBag{i} = OptflowData(indexOPT);
             indexOPT = min(indexOPT+1,length(OptflowData));
             type = [type DataBag{i}.Type];
     end
 end
  disp('<-- Building data bag for isam Done!')
 %% iSAM2 Begin here
 % ******************************************************
disp('-->Begin iSAM')



%% Get initial conditions for the estimated trajectory
FakeInitialPosition = gtsam.Point3(0,0,0);% Suppose the initial p[osition is at 0,0,0, remember to set the GPS data valid (mean begin near 0,0,0)
noiseModelGPS = noiseModel.Diagonal.Precisions([ [0;0;0]; 1.5/1 * [1;1;1] ]);
%noiseModelOptPosition = noiseModel.Diagonal.Precisions([ [0;0;0]; 1/1 * [1;1;1] ]);
%noiseModelOptVel = noiseModel.Isotropic.Sigma(3, 0.1);
%noiseModelOptVel = noiseModel.Diagonal.Precisions([ [0;0;0]; 0.1/1 * [1;1;1] ]);
R_opt_pre=0.1;

currentPoseGlobal = Pose3(Rot3, FakeInitialPosition); % initial pose is the reference frame (navigation frame)
currentVelocityGlobal = LieVector([0;0;0]); % the vehicle is stationary at the beginning
currentBias = imuBias.ConstantBias(zeros(3,1), zeros(3,1));
sigma_init_x = noiseModel.Isotropic.Precisions([ 0.0; 0.0; 0.0; 0.1; 0.1; 0.1 ]);
sigma_init_v = noiseModel.Isotropic.Sigma(3, 0.4);
sigma_init_b = noiseModel.Isotropic.Sigmas([ 0.100; 0.100; 0.100; 5.00e-05; 5.00e-05; 5.00e-05 ]);
sigma_between_b = [ IMU_metadata.AccelerometerBiasSigma * ones(3,1); IMU_metadata.GyroscopeBiasSigma * ones(3,1) ];
g = [0;0;-9.8];
w_coriolis = [0;0;0];

flag_FirstGPS = 1;
Parameter.MAX_EPH_EPV = 20;
Parameter.MIN_EPH_EPV = 2;
Parameter.FLOW_Q_MIN = 0.5;
%% Solver object
isamParams = ISAM2Params;
isamParams.setFactorization('CHOLESKY');
isamParams.setRelinearizeSkip(20);
isam = gtsam.ISAM2(isamParams);
newFactors = NonlinearFactorGraph;
newValues = Values;

%% Main loop:
IMUIntCount = 0;
IMUIntLimt = 20;
IMUInt_MinLim = 2;

flag_RestIMUIntPara = 1;
%flag_FactorRenew = 0; Don't use this flag now. Solver will not update the
%graph evertime. Using this instead:
FactorFramesCount = 1; FactorGraphSize=20;

LatestOptData = [];
lastPoseKey = symbol('x',0);
lastVelKey =  symbol('v',0);
lastBiasKey = symbol('b',0);

w_gps = 1;

disp('----> Starting main loop')


for measurementIndex = 0:TotalDataLength
   % fprintf('____________________________________________\n');
    if measurementIndex == 0
   % fprintf('------> Enter cycle index :%d \n',measurementIndex);
    % inital the factor
        currentPoseKey = symbol('x',measurementIndex);
        currentVelKey =  symbol('v',measurementIndex);
        currentBiasKey = symbol('b',measurementIndex);
        newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
        newFactors.add(PriorFactorLieVector(currentVelKey, currentVelocityGlobal, sigma_init_v));
        newFactors.add(PriorFactorConstantBias(currentBiasKey, currentBias, sigma_init_b));
        newValues.insert(currentPoseKey, currentPoseGlobal);
        newValues.insert(currentVelKey, currentVelocityGlobal);
        newValues.insert(currentBiasKey, currentBias);

    % after one factor created don't forget to set the last key
        lastPoseKey = symbol('x',measurementIndex);
        lastVelKey =  symbol('v',measurementIndex);
        lastBiasKey = symbol('b',measurementIndex);
        
        %FactorFramesCount=FactorFramesCount+1;
        
        fprintf('------># initial Factor created at index :%d \n',measurementIndex);
    else
        CurrentData = DataBag{measurementIndex};
        switch CurrentData.Type
            case 1 % IMU
                LastIMUData = CurrentData;
                if IMUIntCount < IMUIntLimt
                   % Integrate IMU if no limit reached
                   if flag_RestIMUIntPara % this is to do everytime IMU factor created.
                       currentSummarizedMeasurement = gtsam.ImuFactorPreintegratedMeasurements( ...   end % end main loop
                          currentBias, IMU_metadata.AccelerometerSigma.^2 * eye(3), ...
                          IMU_metadata.GyroscopeSigma.^2 * eye(3), IMU_metadata.IntegrationSigma.^2 * eye(3));
                      flag_RestIMUIntPara = 0; %reset the flag
                   end 
                   % Tansform the MAVROS NWU to ENU
                   accMeas = [ -CurrentData.AccY; CurrentData.AccX; CurrentData.AccZ ];
                   omegaMeas = [ -CurrentData.OmegaY; CurrentData.OmegaX; CurrentData.OmegaZ ];
                   
                   deltaT = CurrentData.Dt;
                   currentSummarizedMeasurement.integrateMeasurement(accMeas, omegaMeas, deltaT);
                   IMUIntCount = IMUIntCount + 1; % IMUIntCount add onece
                  % fprintf('------># Do IMU integration at index :%d \n',measurementIndex);
                else % summary the IMU for IMU factor if limit reached
                    currentPoseKey = symbol('x',measurementIndex);
                    currentVelKey =  symbol('v',measurementIndex);
                    currentBiasKey = symbol('b',measurementIndex);
                     % Create IMU factor
                    IMUFctor_Hd = ImuFactor( ...
                    lastPoseKey, lastVelKey, ...
                    currentPoseKey, currentVelKey, ...
                    currentBiasKey, currentSummarizedMeasurement, g, w_coriolis);
                
                    newFactors.add(IMUFctor_Hd);
                    % update cov for IMU
                    sigma_between_b = [ CurrentData.Cov_acc([1 3 9],:); CurrentData.Cov_omega([1 3 9],:) ];
                    
                    % Bias evolution as given in the IMU metadata
                    newFactors.add(BetweenFactorConstantBias(lastBiasKey, currentBiasKey, ...
                        imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
                        noiseModel.Diagonal.Sigmas(sqrt(numel(IMUIntCount)) * sigma_between_b)));
                    % Position factor (Using IMU)
                    newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
                    % dont forget to set last key evrytime the  factor
                    % created
                    lastPoseKey = symbol('x',measurementIndex);
                    lastVelKey =  symbol('v',measurementIndex);
                    lastBiasKey = symbol('b',measurementIndex);
                    
                    % add values
                    newValues.insert(currentPoseKey, currentPoseGlobal);
                    newValues.insert(currentBiasKey, currentBias);
                    newValues.insert(currentVelKey,currentVelocityGlobal);
                    % Clear imu integration 
                    IMUIntCount = 0;
                    flag_RestIMUIntPara = 1;
                    % tell updator to update
                    FactorFramesCount = FactorFramesCount + 1;
                   % fprintf('------># Only IMU Factor created at index :%d \n',measurementIndex);
                end  
            case 2 % GPS
                LastGPSData = CurrentData;
                eph = sqrt(CurrentData.Cov_pose(1,1));
                epv = 0.5*eph;
                if (eph < Parameter.MAX_EPH_EPV * 0.7 && epv <  Parameter.MAX_EPH_EPV * 0.7 && CurrentData.Status >=0)
                    
                    if IMUIntCount > IMUInt_MinLim
                        currentPoseKey = symbol('x',measurementIndex);
                        currentVelKey =  symbol('v',measurementIndex);
                        currentBiasKey = symbol('b',measurementIndex);

                        % First thing first, summary imu integration
                        % Create IMU factor
                        newFactors.add(ImuFactor( ...
                        lastPoseKey, lastVelKey, ...
                        currentPoseKey, currentVelKey, ...
                        currentBiasKey, currentSummarizedMeasurement, g, w_coriolis));
                        % update IMU Cov
                        sigma_between_b = [ LastIMUData.Cov_acc([1 3 9],:); LastIMUData.Cov_omega([1 3 9],:) ];
                        % Bias evolution as given in the IMU metadata
                        newFactors.add(BetweenFactorConstantBias(lastBiasKey, currentBiasKey, ...
                            imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
                            noiseModel.Diagonal.Sigmas(sqrt(numel(IMUIntCount)) * sigma_between_b)));
                        %  prepare posiotion data:
                        % fprintf('------># IMU Factor created at index :%d \n',measurementIndex);
                        if flag_FirstGPS == 1
                            GPSBias = gtsam.Point3(currentPoseGlobal.x,...
                                currentPoseGlobal.y,...
                                currentPoseGlobal.z);
                            GPSVelBias = [CurrentData.Vx;CurrentData.Vy;CurrentData.Vz];
                            flag_FirstGPS = 0;
                        end
                        % update GPS noise model

                        noiseModelGPS = noiseModel.Diagonal.Precisions([ [0;0;0]; [eph;eph;epv] ]);
                        noiseModelGPSVel = noiseModel.Isotropic.Sigma(3, eph);
                        % update GPS pose
                        GPSPose = Pose3(currentPoseGlobal.rotation, gtsam.Point3(...
                            CurrentData.Position.x + GPSBias.x,...
                            CurrentData.Position.y + GPSBias.y,...
                            CurrentData.Position.z + GPSBias.z));
                        % update GPS Vel
                        GPSVelocity = LieVector([CurrentData.Vx+GPSVelBias(1);CurrentData.Vy+GPSVelBias(2);CurrentData.Vz++GPSVelBias(3)]); 
                        % Position factor (Using GPS)
                        newFactors.add(PriorFactorPose3(currentPoseKey, GPSPose, noiseModelGPS));
                        % Position factor (Using IMU)
                        newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
                        % Velocity  factor
                        newFactors.add(PriorFactorLieVector(currentVelKey, GPSVelocity, noiseModelGPSVel));
                        % dont forget to set last key evrytime the  factor
                        % created
                        lastPoseKey = symbol('x',measurementIndex);
                        lastVelKey =  symbol('v',measurementIndex);
                        lastBiasKey = symbol('b',measurementIndex);
                        % add values
                        newValues.insert(currentPoseKey, GPSPose); 
                        newValues.insert(currentBiasKey, currentBias);
                        newValues.insert(currentVelKey,GPSVelocity);
                        % Clear imu integration 
                        IMUIntCount = 0;
                        flag_RestIMUIntPara = 1;
                        % tell updator to update
                        FactorFramesCount = FactorFramesCount + 1;
                        %fprintf('------># GPS Pose is %f %f %f \n',GPSPose.x,GPSPose.y,GPSPose.z);
                        %fprintf('------># GPS Factor with IMU created at index :%d with eph %s \n',measurementIndex,eph);
                    else
                        %fprintf('------># We have GPS signal here but due to too less IMU, no GPS Factor created at %d \n',measurementIndex');
                    end % end if IMUIntCount > IMUInt_MinLim
                else
                end
            case 3 % Opticalflow data
                opt_data = CurrentData;
                flow_q = CurrentData.Quality/255;
                Opt_R = eul2rotm(CurrentData.Euler');
                if flow_q > Parameter.FLOW_Q_MIN && Opt_R(3,3)>0.7
                    if IMUIntCount > IMUInt_MinLim
                        LastOptflowData = CurrentData;

                        currentPoseKey = symbol('x',measurementIndex);
                        currentVelKey =  symbol('v',measurementIndex);
                        currentBiasKey = symbol('b',measurementIndex);

                        % First thing first, summary imu integration
                        % Create IMU factor
                        newFactors.add(ImuFactor( ...
                        lastPoseKey, lastVelKey, ...
                        currentPoseKey, currentVelKey, ...
                        currentBiasKey, currentSummarizedMeasurement, g, w_coriolis));
                        % update IMU Cov
                        sigma_between_b = [ LastIMUData.Cov_acc([1 3 9],:); LastIMUData.Cov_omega([1 3 9],:) ];
                        % Bias evolution as given in the IMU metadata
                        newFactors.add(BetweenFactorConstantBias(lastBiasKey, currentBiasKey, ...
                            imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
                            noiseModel.Diagonal.Sigmas(sqrt(numel(IMUIntCount)) * sigma_between_b)));


                        OptVelocity = currentVelocityGlobal.vector;
                        OptVelocity = LieVector([CurrentData.Vx;CurrentData.Vy;OptVelocity(3)]); 
                        newFactors.add(PriorFactorLieVector(currentVelKey, OptVelocity, noiseModel.Isotropic.Sigma(3, R_opt_pre/flow_q^2)));
                        % Position factor (Using IMU)
                         %newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
                         %newFactors.add(PriorFactorLieVector(currentVelKey, currentVelocityGlobal,sigma_init_v));
                        % dont forget to set last key evrytime the  factor
                        % created
                        lastPoseKey = symbol('x',measurementIndex);
                        lastVelKey =  symbol('v',measurementIndex);
                        lastBiasKey = symbol('b',measurementIndex);
                        % add values
                        newValues.insert(currentPoseKey, currentPoseGlobal); 
                        newValues.insert(currentBiasKey, currentBias);
                        newValues.insert(currentVelKey,OptVelocity);
                        % Clear imu integration 
                        IMUIntCount = 0;
                        flag_RestIMUIntPara = 1;
                        % tell updator to update
                        FactorFramesCount = FactorFramesCount + 1;
                        %fprintf('------># GPS Pose is %f %f %f \n',GPSPose.x,GPSPose.y,GPSPose.z);
                       % fprintf('------># !!!!! OPT Factor with IMU created at index :%d \n',measurementIndex);
                    else
                        LastOptflowData = CurrentData;
                        currentVelKey =  symbol('v',measurementIndex);
                        OptVelocity = currentVelocityGlobal.vector;
                        OptVelocity = LieVector([CurrentData.Vx;CurrentData.Vy;OptVelocity(3)]); 
                        newFactors.add(PriorFactorLieVector(currentVelKey, OptVelocity, noiseModel.Isotropic.Sigma(3, R_opt_pre/flow_q^2)));
                        lastVelKey =  symbol('v',measurementIndex);
                        newValues.insert(currentVelKey,OptVelocity);
                        FactorFramesCount = FactorFramesCount + 1;
                        %fprintf('------># Only OPT Factor created at index :%d \n',measurementIndex);
                    end % end if IMUIntCount > IMUInt_MinLim
                end
        end % switch CurrentData.Type
        % Update solver
        % =======================================================================
        if mod(FactorFramesCount,FactorGraphSize)==0
           % if measurementIndex > 50% TODO!! the bigining point to be determind later
            %  fprintf('------># Solver is going to rock at index :%d, all factor will be cleared \n',measurementIndex);
              isam.update(newFactors, newValues);
              newFactors = NonlinearFactorGraph;
              newValues = Values;

              %if rem(measurementIndex,10)==0 % plot every 10 time steps
%                 cla;
%                 plot3DTrajectory(isam.calculateEstimate, 'r-');
%                 title('Estimated trajectory using ISAM2 (IMU+GPS+Opt)')
%                 xlabel('[m]')
%                 ylabel('[m]')
%                 zlabel('[m]')
%                 axis equal
%                 drawnow;
             % end
              % =======================================================================
              currentPoseGlobal = isam.calculateEstimate(currentPoseKey);
              currentVelocityGlobal = isam.calculateEstimate(currentVelKey);
              currentBias = isam.calculateEstimate(currentBiasKey);
          %  end
            FactorFramesCount = 1; % reset flag
        end % end of if flag_FactorRenew
        
    
    end % end of if measureIndex == 0

   % fprintf('<------ End of cycle index :%d \n',measurementIndex);
   % fprintf('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n\n');
end % end main loop
disp('<---- End of main loop')
%% Plot
figure(1)
plot3DTrajectory(isam.calculateEstimate,'b.');
hold on
plot([GPSData.LocalX],[GPSData.LocalY],'r.');
plot([OptflowData.X],[OptflowData.Y],'g');
title('Fatoc Graph Fusion')
xlabel('[m]')
ylabel('[m]')
zlabel('[m]')
axis equal
hold off

 