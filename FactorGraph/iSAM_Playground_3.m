%% This is the all-plot version of GPS-IMU fusion
% Dat with GPS and IMU and OPT

%% Header and Flags
NeedRereadFile = 1;
clc;
import gtsam.*;
disp('Fusion test on Px4 Using Factor Graph')
%% Raw data reading and pre-process section
% Read data from file
if NeedRereadFile
clear;
disp('-->Begin read data form file.');
    FileIMUAtmData = ReadRosData('IMUAtmData.txt');
    FileIMUDataData = ReadRosData('IMUDataData.txt');
    FileIMUDataRawData = ReadRosData('IMUDataRawData.txt');
    FileIMUMagData = ReadRosData('IMUMagData.txt');
    FileIMUTempData = ReadRosData('IMUTempData.txt');
    FileLocalPostionLocalData = ReadRosData('LocalPostionLocalData.txt');
    FileLocalPostionPoseData = ReadRosData('LocalPostionPoseData.txt');
    FileLocalPostionVelocityData = ReadRosData('LocalPostionVelocityData.txt');
    FilePositionGlobaGlobalData = ReadRosData('PositionGlobaGlobalData.txt');
    FilePositionGlobalCompassData = ReadRosData('PositionGlobalCompassData.txt');
    FilePositionGlobalGpVelData = ReadRosData('PositionGlobalGpVelData.txt');
    FilePositionGlobalLocalData = ReadRosData('PositionGlobalLocalData.txt');
    FilePositionGlobalRawFixData = ReadRosData('PositionGlobalRawFixData.txt');
    FilePositionGlobalRawGPSVelData = ReadRosData('PositionGlobalRawGPSVelData.txt');
    FilePositionGlobalRelAltData = ReadRosData('PositionGlobalRelAltData.txt');
    FilePx4flowOpticalFlowRadData = ReadRosData('Px4flowOpticalFlowRadData.txt');
disp('<--Reading data done.');
end
% Pre-process of data
disp('-->Processing raw structure.')
% IMU data
disp('----> IMU data')
    fn = fieldnames(FileIMUDataData);
    % IMU data type (1)
    t = num2cell(ones(length(FileIMUDataData),1));
    [IMUData(1:length(FileIMUDataData)).Type]=deal(t{:});
    % IMUData Time
    t=num2cell(([FileIMUDataData.IMUDataTime]'-FileIMUDataData(1).IMUDataTime)*(1e-9));
    [IMUData.Time]=deal(t{:});
    % IMUData Dt
    p=[IMUData.Time];q=[0,p];
    t=num2cell(p'-q(1:length(q)-1)');
    [IMUData.Dt]=deal(t{:});
    % Frame Trans:original is x:forward,y:left,z:up.we need to change it to
    % x;left,y:forward,z:up to fit the GPS ENU.
        % This step is only done to Q acc and gyro for now with cov
        % unchanged! TODO:cov
        
    % IMU Orign
    t = cellfun(@(x) x', num2cell([[FileIMUDataData.IMUDataOrienQw]' -[FileIMUDataData.IMUDataOrienQy]'...
        [FileIMUDataData.IMUDataOrienQx]' [FileIMUDataData.IMUDataOrienQz]' ...
        ],2), 'UniformOutput', false); %NOTICE: FRAME TRANS HERE !!!!(x_n=-y_b y_n=x_b)
    [IMUData.Q]=deal(t{:});
     % IMU Orign Cov
    t=[FileIMUDataData.IMUDataOrienCovElems0]';
    for i=[8:15]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_Q]=deal(t{:});
    % IMU acc   
    t=num2cell([FileIMUDataData.IMUDataLinearAccX]');
    [IMUData.AccY]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(y=x)
    t=num2cell(-[FileIMUDataData.IMUDataLinearAccY]');
    [IMUData.AccX]=deal(t{:});  %NOTICE: FRAME TRANS HERE !!!!(x=-y)
    t=num2cell([FileIMUDataData.IMUDataLinearAccZ]');
    [IMUData.AccZ]=deal(t{:}); 
    %IMU acc Cov
    t=[FileIMUDataData.IMUDataLinearAccCovElems0]';
    for i=[32:39]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_acc]=deal(t{:});  
    % IMU Omega   
    t=num2cell([FileIMUDataData.IMUDataAngVelX]');
    [IMUData.OmegaY]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(y=x)
    t=num2cell(-[FileIMUDataData.IMUDataAngVelY]');
    [IMUData.OmegaX]=deal(t{:}); %NOTICE: FRAME TRANS HERE !!!!(x=-y)
    t=num2cell([FileIMUDataData.IMUDataAngVelZ]');
    [IMUData.OmegaZ]=deal(t{:}); 
    %IMU Omega Cov
    t=[FileIMUDataData.IMUDataAngVelCovElems0]';
    for i=[20:27]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_omega]=deal(t{:});      
    % IMU acc-omega (required by isam) %NOTICE: FRAME TRANS HERE !!!!
    t = cellfun(@(x) x', num2cell([...
        -[FileIMUDataData.IMUDataLinearAccY]' [FileIMUDataData.IMUDataLinearAccX]' [FileIMUDataData.IMUDataLinearAccZ]' ...
        -[FileIMUDataData.IMUDataAngVelY]' [FileIMUDataData.IMUDataAngVelX]' [FileIMUDataData.IMUDataAngVelZ]'...
        ],2), 'UniformOutput', false);
   [IMUData.acc_omega] = deal(t{:});
   clear t p q;
% GPS data Here is ENU
disp('----> GPS data')
    fn = fieldnames(FilePositionGlobalRawFixData);
    % GPS data type (2)
    t = num2cell(2*ones(length(FilePositionGlobalRawFixData),1));
    [GPSData(1:length(FilePositionGlobalRawFixData)).Type]=deal(t{:});
    % GPSData Time
    t=num2cell(([FilePositionGlobalRawFixData.PositionGlobalRawFixTime]'-FilePositionGlobalRawFixData(1).PositionGlobalRawFixTime)*(1e-9));
    [GPSData.Time]=deal(t{:});
    % GPS location
    Lat = [FilePositionGlobalRawFixData.PositionGlobalRawFixLat];
    Lon = [FilePositionGlobalRawFixData.PositionGlobalRawFixLon]; 
    Alt = [FilePositionGlobalRawFixData.PositionGlobalRawFixAlt];
    AvrAlt=7.8;%(m)%local alt
    [rm,~,rl]=ellradii(mean(Lat));%get radii of earth
    GPSDataX=deg2rad(Lon).*(rl+AvrAlt);
    GPSDataY=deg2rad(Lat).*(rm+AvrAlt);
    GPSDataZ=Alt-AvrAlt;
    % Frame trnas
    
    % reset cood
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);
    t=num2cell(GPSDataX');
    [GPSData.LocalX]=deal(t{:}); 
    t=num2cell(GPSDataY');
    [GPSData.LocalY]=deal(t{:}); 
    t=num2cell(GPSDataZ');
    [GPSData.LocalZ]=deal(t{:}); 
    % GPS position Cov
    t=[FilePositionGlobalRawFixData.PositionGlobalRawFixPositionCovElems0]';
    for i=[8:15]
        t= [t [FilePositionGlobalRawFixData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [GPSData.Cov_pose]=deal(t{:}); 
    % GPS postion combined used by iSAM
    for i = 1:numel(GPSData)
        GPSData(i).Position = gtsam.Point3(GPSData(i).LocalX, GPSData(i).LocalY, GPSData(i).LocalZ);
    end
       clear fn p q Lat Lon Alt AvrAlt i rl rm t;
% Optialflow data
disp('----> Opticalflow data')
    fn = fieldnames(FilePx4flowOpticalFlowRadData);
    % Optialflow data type (3)
    t = num2cell(3*ones(length(FilePx4flowOpticalFlowRadData),1));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).Type]=deal(t{:});
    % Optflow Time
    t=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadTime]'-FilePx4flowOpticalFlowRadData(1).Px4flowOpticalFlowRadTime)*(1e-9));
    [OptflowData.Time]=deal(t{:});
    % Optflow int time
    dt=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntTime_us]')*(1e-6));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).IntTime]=deal(dt{:});
    % Optflow pose
    dy = [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntX];%NOTICE: FRAME TRANS HERE !!!!
    dx = [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntY];%NOTICE: FRAME TRANS HERE !!!!
    t_imu = [IMUData.Time];
    q_imu = [IMUData.Q];
    t=[OptflowData.Time];
    dt=[OptflowData.IntTime];
    q=zeros(4,length(OptflowData));
    j=1;
    for i=1:length(t_imu)
        if j<=length(OptflowData)
            if t(j)==t_imu(i)
                q(:,j)=q_imu(:,i);
                j=j+1;continue;
            elseif t(j)<t_imu(min(i+1,length(t_imu)))
                q(:,j)=(q_imu(:,i)+q_imu(:,i+1))/2;
                j=j+1;
                continue;
            else
                continue;
            end
        end
    end
    yaw = atan2(2.*q(1,:).*q(4,:)+2.*q(2,:).*q(3,:),1-2.*q(3,:).*q(3,:)-2.*q(4,:).*q(4,:));
    dx_fix = cos(yaw) .* (-1.*dx) - sin(yaw) .* (-1.*dy);
    dy_fix = sin(yaw) .* (-1.*dx) + cos(yaw) .* (-1.*dy);

%     t=quatrotate(q',[dx' dy' zeros(length(dx),1)]);
%     dx_fix=t(:,1);
%     dy_fix=t(:,2);
    
    X=zeros(1,length(dx));X(1)=dx_fix(1);
    Y=X;Y(1)=dy_fix(1);
    for i=2:length(OptflowData)
        X(i)=X(i-1)+dx_fix(i-1);
        Y(i)=Y(i-1)+dy_fix(i-1);
    end
    Vx = dx_fix./dt;Vy = dy_fix./dt;
    X=num2cell(X');Y=num2cell(Y');
    Vx=num2cell(Vx');Vy=num2cell(Vy');
    
    [OptflowData.X]=deal(X{:});
    [OptflowData.Y]=deal(Y{:}); 
    [OptflowData.Vx]=deal(Vx{:});
    [OptflowData.Vy]=deal(Vy{:}); 
 % matadata   
    disp('----> Reading sensor metadata')
    IMU_metadata = importdata('ExampleIMU_metadata');
    IMU_metadata = cell2struct(num2cell(IMU_metadata.data), IMU_metadata.colheaders, 2);
    IMUinBody = Pose3.Expmap([IMU_metadata.BodyPtx; IMU_metadata.BodyPty; IMU_metadata.BodyPtz;
    IMU_metadata.BodyPrx; IMU_metadata.BodyPry; IMU_metadata.BodyPrz; ]);
    if ~IMUinBody.equals(Pose3, 1e-5)
      error 'Currently only support IMUinBody is identity, i.e. IMU and body frame are the same';
    end   
 % Read data end
 disp('<-- Read data end')
 %% Rebuild data bag
 disp('--> Building data bag for isam')
 TotalDataLength  = length(IMUData)+length(GPSData)+length(OptflowData);
 DataBag = cell(TotalDataLength,1); % create the data bag teh same length of all used sensors
 indexIMU=1;indexGPS=1;indexOPT=1;
 type = [];
 for i=1:TotalDataLength
     [~,MinType]=min([IMUData(indexIMU).Time,GPSData(indexGPS).Time,OptflowData(indexOPT).Time]);
     switch MinType
         case 1 % IMU
             DataBag{i} = IMUData(indexIMU);
             indexIMU = min(indexIMU+1,length(IMUData));
             type = [type DataBag{i}.Type];
         case 2 % GPS
             DataBag{i} = GPSData(indexGPS);
             indexGPS = min(indexGPS+1,length(GPSData));
             type = [type DataBag{i}.Type];
         case 3 % Opt
             DataBag{i} = OptflowData(indexOPT);
             indexOPT = min(indexOPT+1,length(OptflowData));
             type = [type DataBag{i}.Type];
     end
 end
  disp('<-- Building data bag for isam Done!')
 %% iSAM2 Begin here
 % ******************************************************
disp('-->Begin iSAM')



%% Get initial conditions for the estimated trajectory
FakeInitialPosition = gtsam.Point3(0,0,0);% Suppose the initial p[osition is at 0,0,0, remember to set the GPS data valid (mean begin near 0,0,0)
noiseModelGPS = noiseModel.Diagonal.Precisions([ [0;0;0]; 1/1 * [1;1;1] ]);
currentPoseGlobal = Pose3(Rot3, FakeInitialPosition); % initial pose is the reference frame (navigation frame)
currentVelocityGlobal = LieVector([0;0;0]); % the vehicle is stationary at the beginning
currentBias = imuBias.ConstantBias(zeros(3,1), zeros(3,1));
sigma_init_x = noiseModel.Isotropic.Precisions([ 0.0; 0.0; 0.0; 1; 1; 1 ]);
sigma_init_v = noiseModel.Isotropic.Sigma(3, 1000.0);
sigma_init_b = noiseModel.Isotropic.Sigmas([ 0.100; 0.100; 0.100; 5.00e-05; 5.00e-05; 5.00e-05 ]);
sigma_between_b = [ IMU_metadata.AccelerometerBiasSigma * ones(3,1); IMU_metadata.GyroscopeBiasSigma * ones(3,1) ];
g = [0;0;-9.8];
w_coriolis = [0;0;0];

GPSBias = gtsam.Point3(0,0,0); % GPS bias got by IMU integration before first GPS point. 

%% Solver object
isamParams = ISAM2Params;
isamParams.setFactorization('CHOLESKY');
isamParams.setRelinearizeSkip(10);
isam = gtsam.ISAM2(isamParams);
newFactors = NonlinearFactorGraph;
newValues = Values;

%% Main loop:
IMUIntCount = 0;
IMUIntLimt = 20;

flag_RestIMUIntPara = 1;
flag_FactorRenew = 0;
flag_FirstGPS = 1;
lastPoseKey = symbol('x',0);
lastVelKey =  symbol('v',0);
lastBiasKey = symbol('b',0);

disp('----> Starting main loop')

for measurementIndex = 0:TotalDataLength
    if measurementIndex == 0
    % inital the factor
        currentPoseKey = symbol('x',measurementIndex);
        currentVelKey =  symbol('v',measurementIndex);
        currentBiasKey = symbol('b',measurementIndex);
        newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
        newFactors.add(PriorFactorLieVector(currentVelKey, currentVelocityGlobal, sigma_init_v));
        newFactors.add(PriorFactorConstantBias(currentBiasKey, currentBias, sigma_init_b));
        newValues.insert(currentPoseKey, currentPoseGlobal);
        newValues.insert(currentVelKey, currentVelocityGlobal);
        newValues.insert(currentBiasKey, currentBias);

    % after one factor created don't forget to set the last key
        lastPoseKey = symbol('x',measurementIndex);
        lastVelKey =  symbol('v',measurementIndex);
        lastBiasKey = symbol('b',measurementIndex);
        
        flag_FactorRenew=1;
        
        fprintf('------># initial Factor created at index :%d \n',measurementIndex);
    else
        CurrentData = DataBag{measurementIndex};
        switch CurrentData.Type
            case 1 % IMU
                if IMUIntCount < IMUIntLimt
                   % Integrate IMU if no limit reached
                   if flag_RestIMUIntPara % this is to do everytime IMU factor created.
                       currentSummarizedMeasurement = gtsam.ImuFactorPreintegratedMeasurements( ...   end % end main loop
                          currentBias, IMU_metadata.AccelerometerSigma.^2 * eye(3), ...
                          IMU_metadata.GyroscopeSigma.^2 * eye(3), IMU_metadata.IntegrationSigma.^2 * eye(3));
                      flag_RestIMUIntPara = 0; %reset the flag
                   end 
                   accMeas = [ CurrentData.AccX; CurrentData.AccY; CurrentData.AccZ ];
                   omegaMeas = [ CurrentData.OmegaX; CurrentData.OmegaY; CurrentData.OmegaZ ];
                   deltaT = CurrentData.Dt;
                   currentSummarizedMeasurement.integrateMeasurement(accMeas, omegaMeas, deltaT);
                   IMUIntCount = IMUIntCount + 1; % IMUIntCount add onece
                else % summary the IMU for IMU factor if limit reached
                    currentPoseKey = symbol('x',measurementIndex);
                    currentVelKey =  symbol('v',measurementIndex);
                    currentBiasKey = symbol('b',measurementIndex);
                     % Create IMU factor
                    newFactors.add(ImuFactor( ...
                    lastPoseKey, lastVelKey, ...
                    currentPoseKey, currentVelKey, ...
                    currentBiasKey, currentSummarizedMeasurement, g, w_coriolis));
                 
                    % Bias evolution as given in the IMU metadata
                    newFactors.add(BetweenFactorConstantBias(lastBiasKey, currentBiasKey, ...
                        imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
                        noiseModel.Diagonal.Sigmas(sqrt(numel(IMUIntCount)) * sigma_between_b)));
                    % Position factor (Using IMU)
                    newFactors.add(PriorFactorPose3(currentPoseKey, currentPoseGlobal, sigma_init_x));
                    % dont forget to set last key evrytime the  factor
                    % created
                    lastPoseKey = symbol('x',measurementIndex);
                    lastVelKey =  symbol('v',measurementIndex);
                    lastBiasKey = symbol('b',measurementIndex);
                    
                    % add values
                    newValues.insert(currentPoseKey, currentPoseGlobal);
                    newValues.insert(currentVelKey, currentVelocityGlobal);
                    newValues.insert(currentBiasKey, currentBias);
                    % Clear imu integration 
                    IMUIntCount = 0;
                    flag_RestIMUIntPara = 1;
                    % tell updator to update
                    flag_FactorRenew = 1;
                    fprintf('------># Only IMU Factor created at index :%d \n',measurementIndex);
                end
            case 2 % GPS
                currentPoseKey = symbol('x',measurementIndex);
                currentVelKey =  symbol('v',measurementIndex);
                currentBiasKey = symbol('b',measurementIndex);

                % First thing first, summary imu integration
                % Create IMU factor
                newFactors.add(ImuFactor( ...
                lastPoseKey, lastVelKey, ...
                currentPoseKey, currentVelKey, ...
                currentBiasKey, currentSummarizedMeasurement, g, w_coriolis));
                % Bias evolution as given in the IMU metadata
                newFactors.add(BetweenFactorConstantBias(lastBiasKey, currentBiasKey, ...
                    imuBias.ConstantBias(zeros(3,1), zeros(3,1)), ...
                    noiseModel.Diagonal.Sigmas(sqrt(numel(IMUIntCount)) * sigma_between_b)));
                %  prepare osiotion data:
                
                if flag_FirstGPS == 1
                    GPSBias = gtsam.Point3(currentPoseGlobal.x,...
                        currentPoseGlobal.y,...
                        currentPoseGlobal.z);
                    flag_FirstGPS = 0;
                end
                GPSPose = Pose3(currentPoseGlobal.rotation, gtsam.Point3(...
                    CurrentData.Position.x + GPSBias.x,...
                    CurrentData.Position.y + GPSBias.y,...
                    CurrentData.Position.z + GPSBias.z));
                % Position factor (Using GPS)
                newFactors.add(PriorFactorPose3(currentPoseKey, GPSPose, noiseModelGPS));
                % dont forget to set last key evrytime the  factor
                % created
                lastPoseKey = symbol('x',measurementIndex);
                lastVelKey =  symbol('v',measurementIndex);
                lastBiasKey = symbol('b',measurementIndex);
                % add values
                newValues.insert(currentPoseKey, GPSPose);
                newValues.insert(currentVelKey, currentVelocityGlobal);
                newValues.insert(currentBiasKey, currentBias);
                % Clear imu integration 
                IMUIntCount = 0;
                flag_RestIMUIntPara = 1;
                % tell updator to update
                flag_FactorRenew = 1;
                fprintf('------># GPS Factor created at index :%d \n',measurementIndex);
            case 3 % Opt
                fprintf('------># SKIP: OPT Factor WILL NOT created at index :%d \n',measurementIndex);
        end % switch CurrentData.Type
        % Update solver
        % =======================================================================
        if flag_FactorRenew
            if measurementIndex > 2*IMUIntLimt % TODO!! the bigining point to be determind later
              isam.update(newFactors, newValues);
              newFactors = NonlinearFactorGraph;
              newValues = Values;

              if rem(measurementIndex,10)==0 % plot every 10 time steps
                cla;
                plot3DTrajectory(isam.calculateEstimate, 'r-');
                title('Estimated trajectory using ISAM2 (IMU+GPS)')
                xlabel('[m]')
                ylabel('[m]')
                zlabel('[m]')
                axis equal
                drawnow;
              end
              % =======================================================================
              currentPoseGlobal = isam.calculateEstimate(currentPoseKey);
              currentVelocityGlobal = isam.calculateEstimate(currentVelKey);
              currentBias = isam.calculateEstimate(currentBiasKey);
            end
            flag_FactorRenew = 0; % reset flag
        end % end of if flag_FactorRenew
        fprintf('------># Position now is %f %f %f \n',currentPoseGlobal.x,currentPoseGlobal.y,currentPoseGlobal.z);
    fprintf('<------ End of index :%d with sensor type %d \n',measurementIndex,type(measurementIndex));
    end % end of if measureIndex == 0
    
end % end main loop
disp('<---- End of main loop')



 