clc;close all;
%% This is the Model-identification based position estimator for multirotors
%% Header and Flags
clear;clc;close all;
NeediSam2 = 0;
disp('Fusion test on Px4 Model-identification based position estimator ')
%% Read Data
run('Fusion_ReadData.m')
%% Rebuild data bag
 disp('--> Building data bag')
 TotalDataLength  = length(IMUData)+length(GPSData)+length(OptflowData);%+length(BaroData);
 type = [];
 DataBag = cell(TotalDataLength,1); % create the data bag teh same length of all used sensors
 indexIMU=1;indexGPS=1;indexOPT=1;indexBaro=1;
 for i=1:TotalDataLength
     [~,MinType]=min([IMUData(indexIMU).Time,GPSData(indexGPS).Time,OptflowData(indexOPT).Time]);%,BaroData(indexBaro).Time]);

     switch MinType
         case 1 % IMU
             DataBag{i} = IMUData(indexIMU);
             indexIMU = min(indexIMU+1,length(IMUData));
             type = [type DataBag{i}.Type];
         case 2 % GPS
             DataBag{i} = GPSData(indexGPS);
             indexGPS = min(indexGPS+1,length(GPSData));
             type = [type DataBag{i}.Type];
        case 3 % Opt
             DataBag{i} = OptflowData(indexOPT);
             indexOPT = min(indexOPT+1,length(OptflowData));
             type = [type DataBag{i}.Type];
%          case 4 % Baro
% 
%              DataBag{i} = BaroData(indexBaro);
%              indexBaro = min(indexBaro+1,length(BaroData));
%              type = [type DataBag{i}.Type];
     end
 end
  disp('<-- Building data bag Done!')
 %% EKF Begin here
 % ******************************************************
disp('-->Begin Model-identification based position estimator for multirotors ');
%% Get initial conditions

CONSTANT_G = 9.81;
 Parameter.FLOW_Q_MIN = 0.5;
 Parameter.MIN_EPH_EPV = 2;
 Parameter.MAX_EPH_EPV = 20;
 
states_num = 9;% P(x y z), V(x y z), bias(ax ay az)
FiltedData = [];
FiltedDataIndex = 0;
partical_number = 200;

X = zeros(states_num,1);
X_particals = zeros(states_num,partical_number);
Xdot_particals = X_particals;
X_particals_pre = X_particals;
P = eye(states_num,states_num);
%Q = 1e-5*eye(states_num);
Q = 0.1;
w = zeros(partical_number,1);
w_rec = [];
outIndex_rec=[];

G= zeros(states_num,6);
R_GPS = [];
R_opt_pre = 0.1;
R_Atm = 0.38;
Current_t = 0;
imu_cnt = 0;

terrain = 0;
Opt_Z=0;  
Ground_dist = [];
R_OPT_dis=0.05;

V_imu = [];
V_imu_now = [0 0 0];
opt_k=[];
% plot_partical = plot(X_particals(1,:),X_particals(2,:),'g.','EraseMode','background');
% hold on
%% Main loop:
disp('----> Starting main loop')
for measurementIndex = 1:TotalDataLength
    CurrentData = DataBag{measurementIndex};
    
    switch CurrentData.Type
        case 1 % IMU fusion 
            Current_dt = CurrentData.Dt;
            acc_now = [CurrentData.AccX CurrentData.AccY CurrentData.AccZ];
            att_now = CurrentData.Q';
            acc_world_now = quatrotate(CurrentData.Q',acc_now);
            acc_world_now(3) = acc_world_now(3)-CONSTANT_G;
            acc_world_corr_now = acc_world_now - X(7:9)';% -bias
            % Prediction
            Xdot_particals(1:3,:) = X_particals(4:6,:);
            for i=1:partical_number
            Xdot_particals(4:6,i) = acc_world_corr_now';
            end
            X_particals_pre = X_particals + Xdot_particals.*Current_dt;
            V_imu_now = V_imu_now + acc_world_now.*Current_dt;
            V_imu = [V_imu; V_imu_now];
        case 11 % baro data
        case 2 %GPS fusion
            gps_now = CurrentData;
            eph = sqrt(CurrentData.Cov_pose(1,1));
            epv = 2*eph;
            R_GPS = diag([eph eph epv eph eph epv]);
            if (eph < Parameter.MAX_EPH_EPV * 0.7 && epv <  Parameter.MAX_EPH_EPV * 0.7 && CurrentData.Status >=0) 
                % Sommon Particals
                for i=1:partical_number
                    X_particals_pre(:,i) = X_particals_pre(:,i) + sqrt(Q)*randn(states_num,1);
                end
               
                % Find Z_hat
                Z = [gps_now.LocalX gps_now.LocalY gps_now.LocalZ gps_now.Vx gps_now.Vy gps_now.Vz]';
                Zhat_partical = zeros(states_num,partical_number);
                 
%                     plot(Z(1),Z(2),'b+');
%                     set(plot_partical,'XData',X_particals_pre(1,:),'YData',X_particals_pre(2,:))
%                    % plot(X_particals(1,:),X_particals(2,:),'r.');
%                     drawnow
%                     axis equal
%                     pause(0.5);
                
                % Get likehood
                for i=1:partical_number
                    Zhat_partical(:,i) = X_particals_pre(:,i);
                    lik = exp(-0.5*(Z-Zhat_partical(1:6,i))'*inv(R_GPS)*(Z-Zhat_partical(1:6,i)) ) + 1e-99;
                    w(i) = lik;
                end
                w=w./sum(w);
                % resampling
                outIndex = systematicR(1:partical_number,w(:));
                X_particals(:,:) = X_particals_pre(:,outIndex);

                X=mean(X_particals,2);

                outIndex_rec=[outIndex_rec outIndex'];
            end
            
        case 3 %optflow data
            opt_data = CurrentData;
            % filter 
            Opt_Z_now=opt_data.Z;
            Opt_Z=0.9*Opt_Z+0.1*(Opt_Z_now);
            
            Opt_Z=terrain+Opt_Z;
            flow_q = CurrentData.Quality/255;
            Opt_R = eul2rotm(CurrentData.Euler');
            if flow_q > Parameter.FLOW_Q_MIN && Opt_R(3,3)>0.7
                % Sommon Particals
                for i=1:partical_number
                    X_particals_pre(3:5,i) = X_particals_pre(3:5,i) + sqrt(Q)*randn(3,1);
                end
               
                % Find Z_hat
                Z = [opt_data.Z opt_data.Vx opt_data.Vy]';
                Zhat_partical = zeros(states_num,partical_number);
                 
%                     plot(Z(1),Z(2),'b+');
%                     set(plot_partical,'XData',X_particals_pre(1,:),'YData',X_particals_pre(2,:))
%                    % plot(X_particals(1,:),X_particals(2,:),'r.');
%                     drawnow
%                     axis equal
%                     pause(0.5);
                
                % Get likehood
                R_opt=1*eye(3);
                R_opt(1,1)=R_OPT_dis;
                R_opt(2:3,2:3) = eye(2).*(R_opt_pre/flow_q^2);
                for i=1:partical_number
                    Zhat_partical(:,i) = X_particals_pre(:,i);
                    lik = exp(-0.5*(Z-Zhat_partical(3:5,i))'*inv(R_opt)*(Z-Zhat_partical(3:5,i)) ) + 1e-99;
                    w(i) = lik;
                end
                w=w./sum(w);
                % resampling
                outIndex = systematicR(1:partical_number,w(:));
                X_particals(:,:) = X_particals_pre(:,outIndex);

                X=mean(X_particals,2);

                outIndex_rec=[outIndex_rec outIndex'];
            end
            
    end
    FiltedData = [FiltedData; X'];
    w_rec = [w_rec w];
    
%         plot(X(1),X(2),'r*');
%         set(plot_partical,'XData',X_particals(1,:),'YData',X_particals(2,:))
%        % plot(X_particals(1,:),X_particals(2,:),'r.');
%         drawnow
%         axis equal
    
end
% hold off
disp('<---- End of main loop')
hold on
   figure(1)
   
   plot([GPSData.LocalX],[GPSData.LocalY],'r.');
   plot([OptflowData.X],[OptflowData.Y],'g');
   plot(FiltedData(:,1),FiltedData(:,2),'.');
   title('Partical Fusion');
   axis equal;
hold off
                    

                 
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
              



 