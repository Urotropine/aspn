clc;close all;
%% This is the Model-identification based position estimator for multirotors
%% Header and Flags
clear;clc;close all;
NeediSam2 = 0;
disp('Fusion test on Px4 Model-identification based position estimator ')
%% Read Data
run('Fusion_ReadData.m')
%% Rebuild data bag
 disp('--> Building data bag')
 TotalDataLength  = length(IMUData)+length(GPSData)+length(OptflowData)+length(BaroData);
 type = [];
 DataBag = cell(TotalDataLength,1); % create the data bag teh same length of all used sensors
 indexIMU=1;indexGPS=1;indexOPT=1;indexBaro=1;
 for i=1:TotalDataLength
     [~,MinType]=min([IMUData(indexIMU).Time,GPSData(indexGPS).Time,OptflowData(indexOPT).Time,BaroData(indexBaro).Time]);

     switch MinType
         case 1 % IMU
             DataBag{i} = IMUData(indexIMU);
             indexIMU = min(indexIMU+1,length(IMUData));
             type = [type DataBag{i}.Type];
         case 2 % GPS
             DataBag{i} = GPSData(indexGPS);
             indexGPS = min(indexGPS+1,length(GPSData));
             type = [type DataBag{i}.Type];
        case 3 % Opt
             DataBag{i} = OptflowData(indexOPT);
             indexOPT = min(indexOPT+1,length(OptflowData));
             type = [type DataBag{i}.Type];
         case 4 % Baro

             DataBag{i} = BaroData(indexBaro);
             indexBaro = min(indexBaro+1,length(BaroData));
             type = [type DataBag{i}.Type];
     end
 end
  disp('<-- Building data bag Done!')
 %% EKF Begin here
 % ******************************************************
disp('-->Begin Model-identification based position estimator for multirotors ');
%% Get initial conditions

CONSTANT_G = 9.81;
 Parameter.FLOW_Q_MIN = 0.5;
 Parameter.MIN_EPH_EPV = 2;
 Parameter.MAX_EPH_EPV = 20;
 
states_num = 9;% P(x y z), V(x y z), bias(ax ay az)
FiltedData = [];
FiltedDataIndex = 0;

X = zeros(states_num,1);
Xdot = X;
P = eye(states_num,states_num);
Q = zeros(6,6);
F= zeros(states_num,states_num);
G= zeros(states_num,6);
R_GPS = [];
R_opt_pre = 0.03;
R_Atm = 0.38;
Current_t = 0;
imu_cnt = 0;

terrain = 0;
Opt_Z=0;  
Ground_dist = [];
R_OPT_dis=0.05;

opt_k=[];
%% Main loop:
disp('----> Starting main loop')
for measurementIndex = 1:TotalDataLength
    CurrentData = DataBag{measurementIndex};
    
    switch CurrentData.Type
        case 1 % IMU fusion 
            Current_dt = CurrentData.Dt;
            acc_now = [CurrentData.AccX CurrentData.AccY CurrentData.AccZ];
            att_now = CurrentData.Q';
            acc_world_now = quatrotate(CurrentData.Q',acc_now);
            acc_world_now(3) = acc_world_now(3)-CONSTANT_G;
            acc_world_corr_now = acc_world_now - X(7:9)';% -bias
            % Prediction using IMU
                % clear vars
                Xdot = zeros(states_num,1);
                F= zeros(states_num,states_num);
                G= zeros(states_num,6);
                % Predicted states estimate
                Xdot(1:3) = X(4:6); % pose_dot = v_now;
                Xdot(4:6) = acc_world_corr_now'; % Vel_dot = a_now;
                %
                F(1:3,4:6) = eye(3);
                F(4:6,7:9) = -eye(3);
                G(4:6,4:6) = eye(3); % here we don't transfore the acc and vel to body-frame.
            X = X + Xdot.*Current_dt;
            F = eye(states_num) + F.*Current_dt;
            G = G.*Current_dt;
            P = F*P*F' + G*Q*G';
            % end prediction
            imu_cnt = imu_cnt+1;
        case 11 % baro data
                Z=CurrentData.Alt;
                Zhat=X(3);
                H = zeros(1,states_num);
                H(1,3)=1;
                K = P*H'*inv(H*P*H'+R_Atm);
                X = X + K*(Z-Zhat);
                P = (eye(states_num)-K*H)*P;

        case 2 %GPS fusion
            gps_now = CurrentData;
            eph = sqrt(CurrentData.Cov_pose(1,1));
            epv = 2*eph;
            if (eph < Parameter.MAX_EPH_EPV * 0.7 && epv <  Parameter.MAX_EPH_EPV * 0.7 && CurrentData.Status >=0) 
                % Find Z_hat
                Z = [gps_now.LocalX gps_now.LocalY gps_now.LocalZ gps_now.Vx gps_now.Vy gps_now.Vz]';
                Zhat = zeros(6,1);
                Zhat = X(1:6);
                H = zeros(3,states_num);
                H(1:3,1:3) = eye(3);
                H(4:6,4:6) = eye(3);
                R_GPS = diag([eph eph epv eph eph epv]);
                % Kalman Gain & update
                K = P*H'*inv(H*P*H'+R_GPS);
                X = X + K*(Z-Zhat);
                P = (eye(states_num)-K*H)*P;
                
            end
        case 3 %optflow data
            opt_data = CurrentData;
            % filter 
            Opt_Z_now=opt_data.Z;
            Opt_Z=0.9*Opt_Z+0.1*(Opt_Z_now);
            
            Opt_Z=terrain+Opt_Z;
            flow_q = CurrentData.Quality/255;
            Opt_R = eul2rotm(CurrentData.Euler');
            if flow_q > Parameter.FLOW_Q_MIN && Opt_R(3,3)>0.7
                Z = [Opt_Z opt_data.Vx opt_data.Vy]';
                Zhat = X(3:5);
                H = zeros(3,states_num);
                H(:,3:5) = eye(3); 
                R_opt=1*eye(3);
                R_opt(1,1)=R_OPT_dis;
                R_opt(2:3,2:3) = eye(2).*(R_opt_pre/flow_q^2);

                K = P*H'*inv(H*P*H'+R_opt);
                X = X + K*(Z-Zhat);
                P = (eye(states_num)-K*H)*P;
                opt_k =[opt_k K];
            end
    end

    FiltedData = [FiltedData X];
end
disp('<---- End of main loop')
hold on
   figure(1)
   
   plot([GPSData.LocalX],[GPSData.LocalY],'r.');
   plot([OptflowData.X],[OptflowData.Y],'g');
   plot(FiltedData(1,:),FiltedData(2,:),'.');
   title('EFK Fusion');
   axis equal;
hold off
                    

                 
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
              



 