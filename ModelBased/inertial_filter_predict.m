function [ pos,vel ] = inertial_filter_predict( dt, pos, vel, acc )
if isfinite(dt)
    if ~prod(isfinite(acc))
        acc=0;
    end
    pos = pos + vel.*dt+acc.*dt^2/2;
    vel = vel + acc*dt;
end
end

