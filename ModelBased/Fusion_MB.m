%% This is the Model-identification based position estimator for multirotors
%% Header and Flags
clear;clc;close all;
NeediSam2 = 0;
disp('Fusion test on Px4 Model-identification based position estimator ')
%% Read Data
run('Fusion_ReadData.m')
 %% Rebuild data bag
 disp('--> Building data bag')
 TotalDataLength  = length(IMUData)+length(GPSData)+length(OptflowData)+length(BaroData);
 type = [];
 DataBag = cell(TotalDataLength,1); % create the data bag teh same length of all used sensors
 indexIMU=1;indexGPS=1;indexOPT=1;indexBaro=1;
 for i=1:TotalDataLength
     [~,MinType]=min([IMUData(indexIMU).Time,GPSData(indexGPS).Time,OptflowData(indexOPT).Time,BaroData(indexBaro).Time]);
     switch MinType
         case 1 % IMU
             DataBag{i} = IMUData(indexIMU);
             indexIMU = min(indexIMU+1,length(IMUData));
             type = [type DataBag{i}.Type];
         case 2 % GPS
             DataBag{i} = GPSData(indexGPS);
             indexGPS = min(indexGPS+1,length(GPSData));
             type = [type DataBag{i}.Type];
        case 3 % Opt
             DataBag{i} = OptflowData(indexOPT);
             indexOPT = min(indexOPT+1,length(OptflowData));
             type = [type DataBag{i}.Type];
         case 4 % Baro
             DataBag{i} = BaroData(indexBaro);
             indexBaro = min(indexBaro+1,length(BaroData));
             type = [type DataBag{i}.Type];
     end
 end
  disp('<-- Building data bag for isam Done!')
 %% Model based fusioni Begin here
 % ******************************************************
 last_time = 0;
 % Parameters 
 Parameter.SONAR_FILT = 0.05;
 Parameter.SONAR_ERR = 0.5;% m
 Parameter.MAX_FLOW = 1;
 Parameter.FLOW_Q_MIN = 0.5;
 Parameter.MIN_EPH_EPV = 2;
 Parameter.MAX_EPH_EPV = 20;
 Parameter.GPS_DELAY = 1;%in sensor data conunts 
 %Parameter.AVR_DT = 0.01;
 Parameter.W_XY_GPS_P = 1;
 Parameter.W_XY_GPS_V = 1;
 Parameter.W_Z_GPS_P = 1;
 Parameter.W_Z_GPS_V = 1;
 Parameter.MIN_VALID_W = 0.00001;
 Parameter.W_Z_SONAR = 0.5; % TO CHANGE!!!
 Parameter.W_Z_BARO = 0.1;
 Parameter.W_GPS_FLOW = 0.9;
 Parameter.W_ACC_BIAS = 0.05;
 Parameter.W_XY_FLOW = 0.6;
 
 FLT_EPSILON = 1.1920929e-07;
 MIN_VALID_W = 0.00001;
 CONSTANT_G = 9.80665;
 % States 
 States.Position = zeros(TotalDataLength,3);
 States.Velocity = zeros(TotalDataLength,3);
 States.AccCorr = zeros(TotalDataLength,3);
 States.AngVel = zeros(TotalDataLength,3);
 States.Att = zeros(TotalDataLength,4);States.Att(:,1)=1;
 States.bias.acc = zeros(TotalDataLength,3);
 
 % General varibales (& IMU)
 dt=0;
 
 imu_now = IMUData(1);
 acc_now = [0 0 0];
 acc_bias_now = [0 0 0];
 position_now = [0 0 0];
 velocity_now = [0 0 0];
 att_now = [1 0 0 0];
 att_speed = [0 0 0];% roll  pith yaw
 
 body_v_est = [0 0 0];
 
 % Barro var
 corr_baro = 0;
 baro_offset = 0;
 % Optical flow vars
 opt_now = OptflowData(1);
 surface_offset = 0;
 surface_offset_rate = 0;
 sonar_prev = 0;
 corr_sonar = 0;
 corr_sonar_filtered = 0;
 sonar_valid = 0;
 dist_bottom = 0;
 
 flow_v =[0 0];
 flow_v_corr =[0 0];
 flow_q = 0;
 flow_dist = 0;
 flow_ang = [0 0 0];
 flow_q_weight = 0;
 w_flow = 0;
 eph_flow = 0;
 
 % GPS vars
 eph = 0;
 epv = 0;
 gps_now = GPSData(1);
 gps_valid = 0;
 reset_est = 0;
 ref_inited = 0;
 gps_position_corr = [0 0 0];%corr_gps
 gps_vel_corr = [0 0 0];
 w_gps_xy = 0;
 w_gps_z = 0;
 
 use_flow = 0;
 flow_valid = 0;

 tt=[];
 % Plot varibles 
fprintf('____________________________________________\n');
fprintf('--> Begin main cycle\n');
 for measurementIndex = 1:TotalDataLength
   %fprintf('____________________________________________\n');
   %fprintf('--> Enter cycle index :%d \n',measurementIndex);
   DataNow = DataBag{measurementIndex};
   switch DataNow.Type
       case 1 % IMU
           imu_now = DataNow;
           % correct acc bias
           acc_now = [DataNow.AccX DataNow.AccY DataNow.AccZ];
           acc_now = acc_now-acc_bias_now;
           angvel_now = [DataNow.OmegaX DataNow.OmegaY DataNow.OmegaZ];
           att_now = DataNow.Q';
           % transform acceleration vector from body frame to NED frame 
           acc_now = quatrotate(DataNow.Q',acc_now);
           angvel_now = quatrotate(DataNow.Q',angvel_now);
           acc_now(3) = acc_now(3) - CONSTANT_G;
       case 11 % Baro
           baro_now = DataNow.Alt;
           corr_baro =  baro_now - States.Position(measurementIndex,3);
       case 2 % GPS
           gps_now = DataNow;
           eph = sqrt(DataNow.Cov_pose(1,1));
           epv = 0.5*eph;
           reset_est = 0;
           if gps_valid
                if (eph > Parameter.MAX_EPH_EPV || epv > Parameter.MAX_EPH_EPV || DataNow.Status < 0) 
                    gps_valid = false;
                    fprintf('----> GPS not avalible at cycle :%d \n',measurementIndex);
                end
           else
                if (eph < Parameter.MAX_EPH_EPV * 0.7 && epv <  Parameter.MAX_EPH_EPV * 0.7 && DataNow.Status >=0) 
                    fprintf('----> GPS reset at cycle :%d \n',measurementIndex);
                    gps_valid = true;
                    reset_est = true;
                end
           end
           if gps_valid
               if ~ref_inited
                ref_inited = true;
                %set position estimate to (0, 0, 0), use GPS velocity for XY */
                position_now(1) = 0;
                velocity_now(1) = 0;
                position_now(2) = 0;
                velocity_now(2) = 0;
               end
               if ref_inited
                   if reset_est 
%                         position_now(1) = 0;
%                         velocity_now(1) = 0;
%                         position_now(2) = 0;
%                         velocity_now(2) = 0;
                   end
                   %calculate correction for position *
                   gps_position_corr(1:2) = [DataNow.LocalX-States.Position(max(1,(measurementIndex- Parameter.GPS_DELAY)),1) ...
                       DataNow.LocalY-States.Position(max(1,measurementIndex- Parameter.GPS_DELAY),2)];
                   gps_position_corr(3) = position_now(3)-States.Position(max(1,measurementIndex- Parameter.GPS_DELAY),3);
                   % vel
                   gps_vel_corr = [DataNow.Vx-States.Velocity(max(1,measurementIndex- Parameter.GPS_DELAY),1) ...
                       DataNow.Vx-States.Velocity(max(1,measurementIndex- Parameter.GPS_DELAY),2) ...
                       DataNow.Vz-States.Velocity(max(1,measurementIndex- Parameter.GPS_DELAY),3)];
                   
                   w_gps_xy = Parameter.MIN_EPH_EPV / max(Parameter.MIN_EPH_EPV, eph);
                  
                   w_gps_z	= Parameter.MIN_EPH_EPV / max(Parameter.MIN_EPH_EPV, epv);
               end
           else
               gps_position_corr=[0 0 0];
               gps_vel_corr = [0 0 0];
           end
           
       case 3 % OPT 
           opt_data = DataNow;
           Opt_R = eul2rotm(DataNow.Euler');
%            if DataNow.Z>0.31 && DataNow.Z<4 && Opt_R(3,3)>0.7 &&(DataNow.Z-sonar_prev)>FLT_EPSILON
%                sonar_prev = DataNow.Z;
%                corr_sonar = DataNow.Z + surface_offset + position_now(3);
%                corr_sonar_filtered = corr_sonar_filtered+ (corr_sonar - corr_sonar_filtered) * Parameter.SONAR_FILT;
%                if abs(corr_sonar) >  Parameter.SONAR_ERR; %correction is too large: spike or new ground level? 
%                    if abs(corr_sonar - corr_sonar_filtered) >  Parameter.SONAR_ERR %/* spike detected, ignore */
%                         corr_sonar = 0.0;
%                         sonar_valid = 0;
%                    else
%                        	%new ground level */
%                         surface_offset = surface_offset- corr_sonar;
%                         surface_offset_rate = 0;
%                         corr_sonar = 0;
%                         corr_sonar_filtered = 0;
%                         %sonar_valid_time = t;
%                         sonar_valid = true;
%                    end
%                else
%                    	%correction is ok, use it */
%                     %sonar_valid_time = t;
%                     sonar_valid = true;
%                end
%            end 
           flow_q = DataNow.Quality/255;
%            dist_bottom = dist_bottom - position_now(3) - surface_offset;
%            if dist_bottom > 0.3 && flow_q > Parameter.FLOW_Q_MIN && Opt_R(3,3)>0.7
           if flow_q > Parameter.FLOW_Q_MIN && Opt_R(3,3)>0.7
               % distance to surface !! Here because i don't want to
               % correct ground level now, so jsus take the sonnar as the
               % distance to ground, and ASSUME the ground is FLAT.
               %flow_dist = dist_bottom / Opt_R(3,3);
               flow_dist = DataNow.Z / Opt_R(3,3);
               %check if flow if too large for accurate measurements 
               %calculate estimated velocity in body frame
               
               %body_v_est = (Opt_R*velocity_now')';
               body_v_est = velocity_now;
               %set this flag if flow should be accurate according to current velocity and attitude rate estimate
               
%                flow_accurate = (abs(body_v_est(2) / flow_dist - att_speed(1)) <  Parameter.MAX_FLOW) && ...
%                    (abs(body_v_est(1) / flow_dist + att_speed(2))) <  Parameter.MAX_FLOW;
               % Don't know what is going on up ^^^^^^^^
               % So use blow instead, temperorily. 
               flow_accurate = 1;
               %convert raw flow to angular flow (rad/s)
                flow_v = [DataNow.Vx DataNow.Vy];
               % velocity correction */
                flow_v_corr = flow_v - velocity_now(1:2);
               % adjust correction weight */
               flow_q_weight = (flow_q - Parameter.FLOW_Q_MIN) / (1.0 - Parameter.FLOW_Q_MIN);
               w_flow = Opt_R(3,3) * flow_q_weight / max(1, flow_dist);
              
               %if flow is not accurate, reduce weight for it */
               %TODO make this more fuzzy
                if ~flow_accurate
                    w_flow = w_flow*0.05;
                end
               % under ideal conditions, on 1m distance assume EPH = 10cm */
               eph_flow = 0.1 / w_flow;
               flow_valid = true;
           else
               w_flow = 0;
               flow_valid = false;
           end
   end
   dt = max(min(abs(DataNow.Time-last_time),0.02),0.002);
   % increase EPH/EPV on each step */
   if eph < Parameter.MAX_EPH_EPV
       eph =eph*( 1.0 + dt);
   end
   if epv < Parameter.MAX_EPH_EPV
       epv = epv+0.005 * dt;	% add 1m to EPV each 200s (baro drift)
   end
   
   % use GPS if it's valid and reference position initialized */
   use_gps_xy = ref_inited && gps_valid && Parameter.W_XY_GPS_P > Parameter.MIN_VALID_W;
   use_gps_z = ref_inited && gps_valid && Parameter.W_Z_GPS_P > MIN_VALID_W;
   %use flow if it's valid and (accurate or no GPS available) */
   use_flow = flow_valid && (flow_accurate || ~use_gps_xy);
   can_estimate_xy = (eph < Parameter.MAX_EPH_EPV) || use_gps_xy || use_flow;% || use_vision_xy;
   %dist_bottom_valid = (t < sonar_valid_time + sonar_valid_timeout);
   %if (dist_bottom_valid)
        %/* surface distance prediction */
   surface_offset = surface_offset + surface_offset_rate * dt;
        %/* surface distance correction */
    if sonar_valid
        surface_offset_rate = surface_offset_rate- corr_sonar * 0.5 * Parameter.W_Z_SONAR * Parameter.W_Z_SONAR * dt;
        surface_offset = surface_offset - corr_sonar * Parameter.W_Z_SONAR * dt;
    end
    

    w_xy_gps_p = Parameter.W_XY_GPS_P * w_gps_xy;
    w_xy_gps_v = Parameter.W_XY_GPS_V * w_gps_xy;
    w_z_gps_p = Parameter.W_Z_GPS_P * w_gps_z;
    w_z_gps_v = Parameter.W_Z_GPS_V * w_gps_z;
   
    %/* reduce GPS weight if optical flow is good */
    if use_flow && flow_accurate
        w_xy_gps_p = w_xy_gps_p * Parameter.W_GPS_FLOW;
        w_xy_gps_v = w_xy_gps_v * Parameter.W_GPS_FLOW;
    end

	% /* baro offset correction */
    % 		if use_gps_z
    % 			offs_corr = corr_gps[2][0] * w_z_gps_p * dt;
    % 			baro_offset += offs_corr;
    %         end

    %/* accelerometer bias correction for GPS (use buffered rotation matrix) */
    accel_bias_corr = [0 0 0];

    if (use_gps_xy)
        accel_bias_corr(1) = accel_bias_corr(1) - gps_position_corr(1) * w_xy_gps_p * w_xy_gps_p;
        accel_bias_corr(1) = accel_bias_corr(1) - gps_vel_corr(1) * w_xy_gps_v;
        accel_bias_corr(2) = accel_bias_corr(2) - gps_position_corr(2) * w_xy_gps_p * w_xy_gps_p;
        accel_bias_corr(2) = accel_bias_corr(2) - gps_vel_corr(2) * w_xy_gps_v;
        %fprintf('--> at cycle %d, accel_bias_corr = (%s, %s)\n',measurementIndex,accel_bias_corr(1),accel_bias_corr(2));
    end
    if (use_gps_z)
        accel_bias_corr(3) = accel_bias_corr(3) - gps_position_corr(3) * w_z_gps_p * w_z_gps_p;
        accel_bias_corr(3) = accel_bias_corr(3) - gps_vel_corr(3) * w_z_gps_v;
    end
    acc_bias_now = acc_bias_now + accel_bias_corr.*Parameter.W_ACC_BIAS*dt;
% 		/* accelerometer bias correction for flow and baro (assume that there is no delay) */
		accel_bias_corr = [0 0 0];

    if use_flow
        accel_bias_corr(1:2) = accel_bias_corr(1:2) - flow_v_corr * Parameter.W_XY_FLOW;
    end
    acc_bias_now = acc_bias_now + accel_bias_corr.*Parameter.W_ACC_BIAS*dt;
    
%     /* inertial filter prediction for altitude */
    [position_now(3), velocity_now(3)] = inertial_filter_predict(dt, position_now(3), velocity_now(3), acc_now(3));
% No Baro now
% /* inertial filter correction for altitude */
        [position_now(3), velocity_now(3)] = inertial_filter_correct(corr_baro, dt, position_now(3), velocity_now(3),'pos', Parameter.W_Z_BARO);
    if (use_gps_z)
        epv = min(epv, 0.5*sqrt(gps_now.Cov_pose(1,1)));

        [position_now(3), velocity_now(3)] = inertial_filter_correct(gps_position_corr(3), dt, position_now(3), velocity_now(3),'pos', w_z_gps_p);
        [position_now(3), velocity_now(3)] = inertial_filter_correct(gps_vel_corr(3), dt, position_now(3), velocity_now(3), 'vel', w_z_gps_v);
    end
% 
% 		if (use_vision_z) {
% 			epv = fminf(epv, epv_vision);
% 			inertial_filter_correct(corr_vision[2][0], dt, z_est, 0, w_z_vision_p);
% 		}
% 

   if (can_estimate_xy)
% 			/* inertial filter prediction for position */
        [position_now(1:2), velocity_now(1:2)] = inertial_filter_predict(dt, position_now(1:2), velocity_now(1:2), acc_now(1:2));
        
        if (~(isfinite(position_now(1)) && isfinite(position_now(2)) && isfinite(velocity_now(1)) && isfinite(velocity_now(2)))) 
            fprintf('--> BAD ESTIMATE AFTER PREDICTION at cycle %d \n', measurementIndex);
        end
% 			/* inertial filter correction for position */
        if use_flow
            eph = min(eph, eph_flow);
            [posirion_now(1:2),velocity_now(1:2)] = inertial_filter_correct(flow_v_corr, dt, position_now(1:2), velocity_now(1:2),'vel', Parameter.W_XY_FLOW*w_flow);
        end
        if use_gps_xy
            eph = min(eph, sqrt(gps_now.Cov_pose(1,1)));
            
            [position_now(1:2), velocity_now(1:2)] = inertial_filter_correct(gps_position_corr(1:2), dt, position_now(1:2), velocity_now(1:2),'pos', w_xy_gps_p);

            if gps_valid

              [position_now(1:2), velocity_now(1:2)] = inertial_filter_correct(gps_position_corr(1:2), dt, position_now(1:2), velocity_now(1:2),'vel', w_xy_gps_v);
            end
        end
% 
% 			if (use_vision_xy) {
% 				eph = fminf(eph, eph_vision);
% 
% 				inertial_filter_correct(corr_vision[0][0], dt, x_est, 0, w_xy_vision_p);
% 				inertial_filter_correct(corr_vision[1][0], dt, y_est, 0, w_xy_vision_p);
% 
% 				if (w_xy_vision_v > MIN_VALID_W) {
% 					inertial_filter_correct(corr_vision[0][1], dt, x_est, 1, w_xy_vision_v);
% 					inertial_filter_correct(corr_vision[1][1], dt, y_est, 1, w_xy_vision_v);
% 				}
% 			}
% 
        if ~(isfinite(position_now(1)) && isfinite(position_now(2)) && isfinite(velocity_now(1)) && isfinite(velocity_now(2)))
            fprintf('BAD ESTIMATE AFTER CORRECTION\n');
        end
% 				write_debug_log("BAD ESTIMATE AFTER CORRECTION", dt, x_est, y_est, z_est, x_est_prev, y_est_prev, z_est_prev, acc, corr_gps, w_xy_gps_p, w_xy_gps_v);
% 				memcpy(x_est, x_est_prev, sizeof(x_est));
% 				memcpy(y_est, y_est_prev, sizeof(y_est));
% 				memset(corr_gps, 0, sizeof(corr_gps));
% 				memset(corr_vision, 0, sizeof(corr_vision));
% 				memset(corr_flow, 0, sizeof(corr_flow));
% 
% 			} else {
% 				memcpy(x_est_prev, x_est, sizeof(x_est));
% 				memcpy(y_est_prev, y_est, sizeof(y_est));
% 			}
% 		} else {

   else
       % /* gradually reset xy velocity estimates */
    [position_now(1:2), velocity_now(1:2)] = inertial_filter_correct(gps_position_corr(1:2), dt, position_now(1:2), velocity_now(1:2),'vel', w_xy_gps_v);
   end
    States.Position(measurementIndex,:) = position_now;
    States.Velocity(measurementIndex,:) = velocity_now;
    States.AccCorr(measurementIndex,:) = acc_now;
    States.AngVel(measurementIndex,:) = angvel_now;
    States.Att(measurementIndex,:) = att_now;
    %tt = [tt; quatrotate(att_now,)];
   last_time = DataNow.Time;
 end
 % Plot
hold on
   figure(1)
   
   plot([GPSData.LocalX],[GPSData.LocalY],'r.');
   plot([OptflowData.X],[OptflowData.Y],'g');
   plot(States.Position(:,1),States.Position(:,2),'.');
   title('Model-Based Fusion');
   axis equal;
hold off
disp('<-- End of Model-identification based position estimator for multirotors ');

%    figure()
%    plot(1:length(States.Velocity),States.Velocity(:,1),1:length(States.Velocity),States.Velocity(:,2));
%   title('Velocity');
%   axis equal;
% t=t'
% figure()
% plot(t(:,1:2),'DisplayName','t(:,1:2)')
 