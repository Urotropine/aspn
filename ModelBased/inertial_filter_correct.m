function [ pos_out, vel_out ] = inertial_filter_correct( e, dt, pos, vel, type, w )
%INERTIAL_FILTER_CORRECT Summary of this function goes here
%   Detailed explanation goes here
if prod(isfinite(e) & isfinite(w) & isfinite(dt))
    ewdt = e.*w*dt;
    if strcmp(type,'pos')
        pos_out = pos + ewdt;
        vel_out = vel + w*ewdt;
    else % 'vel'
        pos_out = pos;
        vel_out = vel + ewdt;
    end
end
end

