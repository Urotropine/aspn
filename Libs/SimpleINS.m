function [ output_args ] = SimpleINS( IMU_data )
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
g=9.81;
IniPosition=[0 0 0];
DCM=eye(3);
AngVect_B=[[IMU_data.omegaX]' [IMU_data.omegaY]' [IMU_data.omegaZ]'];
Acc_B=[[IMU_data.accelX]' [IMU_data.accelY]' [IMU_data.accelZ]'];
Dt=[IMU_data.dt]'./1000;
Acc_I=zeros(length(IMU_data),3);
Vect_I=zeros(length(IMU_data),3);
Position=zeros(length(IMU_data),3);
for i=1:(length(IMU_data)-1)
    
    DCM=bodupdat(DCM,AngVect_B(i,:));
    Acc_I(i,:)=(DCM*(Acc_B(i,:)'))';
    Acc_I(i,3)=Acc_I(i,3)+g;
    Vect_I(i+1,:)=Vect_I(i,:)+Acc_I(i,:).*Dt(i);
    Position(i+1,:)=Position(i,:)+Vect_I(i+1,:).*Dt(i);
end
plot(Position(:,1),Position(:,2));
output_args=Position;

