function [ GPSDataX, GPSDataY, GPSDataZ ] = gps2xyz( longitude,latitude,altitude )
%GPS2XYZ Summary of this function goes here
%   Detailed explanation goes here
 AvrAlt=7.8;%(m)%local alt
        [rm,~,rl]=ellradii(mean(latitude));%get radii of earth
        GPSDataX=deg2rad(longitude).*(rl+AvrAlt);
        GPSDataY=deg2rad(latitude).*(rm+AvrAlt);
        GPSDataZ=altitude-AvrAlt;
        
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);

end

