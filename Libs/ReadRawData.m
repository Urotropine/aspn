function [ output_args ] = ReadRawData( DatasetType,Datafile,SensorType, OutCoordinate )
%%READRAWDATA Summary of this function goes here
%   Read data from data set file; support Pixhawk(logged by QGroundControl)
%   iPhone(SensorLog) and the demo from Kitt. 
%   
%   DatasetType:    PX4_QGC/iPhone_SL/iPhone_SL_Opt/GTSAM_Demo
%   Datafile:       Remenber to add the path to MATLAB
%   SensorType:     IMU6/IMU9/GPS/IMU6Only/IMU9Only
%       The IMU6/9Only type will not align the time of IMU and GPS. That's the
%       only different.
%   OutCoordinate:  Raw/NED/NEU/ENU/END/WSD
%       Raw means use the original coordinate the data has (for now:
%       PX4_QGC[NED],iPhone_SL[WSD],GTSAM_Demo[Unknown,maby NWU])
%  
import gtsam.*;

assert(nargin>0,'Please input arguments');
assert(nargin>2,'Please select the data type');
assert(nargin>3,'Please select the output coordinate system from ''Raw'' ''NED''');
%%  Reading for different data source
if strcmp(DatasetType,'PX4_QGC')
    %% Read data file from PX4 with Qgroundcontrl
    RawData=importdata(Datafile);
    RawData = cell2struct(num2cell(RawData.data), RawData.colheaders, 2);
        %% Chop the data if needed
        %ChopRate=0.08;
        %RawData = RawData(floor(ChopRate.*length(RawData)):floor((1-ChopRate).*length(RawData)));
    %% Set up Time array
    if (~strcmp(SensorType,'IMU6Only'))&&(~strcmp(SensorType,'IMU9Only'))
        GPSTimeArray_us=[RawData.M1GPSRAWINTtimeusec];
        FirstGPSTimeIndex=find(sum(~isnan([RawData.M1GPSRAWINTtimeusec]),1) > 0, 1 ,'first');
        GPSTimeArray_us=GPSTimeArray_us(:,all(~isnan(GPSTimeArray_us),length(GPSTimeArray_us)));
        GPSTimeArray=GPSTimeArray_us./1000000;
    end
    IMUTimeArray_ms=[RawData.TIMESTAMPms];
    if (~strcmp(SensorType,'IMU6Only'))&&(~strcmp(SensorType,'IMU9Only'))
        FirstIMUTime_ms = GPSTimeArray(1).*1000-IMUTimeArray_ms(FirstGPSTimeIndex);
        IMUTimeArray_ms=FirstIMUTime_ms+IMUTimeArray_ms;
    end
    tIMUDataArray=[RawData.M1HIGHRESIMUxacc];
    IMUTimeArray_ms=IMUTimeArray_ms(:,all(~isnan(tIMUDataArray),length(tIMUDataArray)));
    IMUTimeArray=IMUTimeArray_ms./1000;
    IMUDtArray=IMUTimeArray;
    for i=2:length(IMUTimeArray)
        IMUDtArray(i)=IMUTimeArray(i)-IMUTimeArray(i-1);
    end
    %% Porcess the IMU data from raw array, including coordination tranfrom
    if strcmp(SensorType,'IMU6')||strcmp(SensorType,'IMU9')||strcmp(SensorType,'IMU6Only')||strcmp(SensorType,'IMU9Only')
        if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'NED')%PX4_QGC
            IMUDataXAcc=[RawData.M1HIGHRESIMUxacc];IMUDataXAcc=IMUDataXAcc(:,all(~isnan(IMUDataXAcc),length(IMUDataXAcc)));
            IMUDataYAcc=[RawData.M1HIGHRESIMUyacc];IMUDataYAcc=IMUDataYAcc(:,all(~isnan(IMUDataYAcc),length(IMUDataYAcc)));
            IMUDataZAcc=[RawData.M1HIGHRESIMUzacc];IMUDataZAcc=IMUDataZAcc(:,all(~isnan(IMUDataZAcc),length(IMUDataZAcc)));
            IMUDataXGyro=[RawData.M1HIGHRESIMUxgyro];IMUDataXGyro=IMUDataXGyro(:,all(~isnan(IMUDataXGyro),length(IMUDataXGyro)));
            IMUDataYGyro=[RawData.M1HIGHRESIMUygyro];IMUDataYGyro=IMUDataYGyro(:,all(~isnan(IMUDataYGyro),length(IMUDataYGyro)));
            IMUDataZGyro=[RawData.M1HIGHRESIMUzgyro];IMUDataZGyro=IMUDataZGyro(:,all(~isnan(IMUDataZGyro),length(IMUDataZGyro)));
            IMUDataXMag=[RawData.M1HIGHRESIMUxmag];IMUDataXMag=IMUDataXMag(:,all(~isnan(IMUDataXMag),length(IMUDataXMag)));
            IMUDataYMag=[RawData.M1HIGHRESIMUymag];IMUDataYMag=IMUDataYMag(:,all(~isnan(IMUDataYMag),length(IMUDataYMag)));
            IMUDataZMag=[RawData.M1HIGHRESIMUzmag];IMUDataZMag=IMUDataZMag(:,all(~isnan(IMUDataZMag),length(IMUDataZMag)));
        elseif strcmp(OutCoordinate,'NWU')%GTSAM_Demo[Unknown,maby NWU]
            IMUDataXAcc=[RawData.M1HIGHRESIMUxacc];IMUDataXAcc=IMUDataXAcc(:,all(~isnan(IMUDataXAcc),length(IMUDataXAcc)));
            IMUDataYAcc=-[RawData.M1HIGHRESIMUyacc];IMUDataYAcc=IMUDataYAcc(:,all(~isnan(IMUDataYAcc),length(IMUDataYAcc)));
            IMUDataZAcc=-[RawData.M1HIGHRESIMUzacc];IMUDataZAcc=IMUDataZAcc(:,all(~isnan(IMUDataZAcc),length(IMUDataZAcc)));
            IMUDataXGyro=[RawData.M1HIGHRESIMUxgyro];IMUDataXGyro=IMUDataXGyro(:,all(~isnan(IMUDataXGyro),length(IMUDataXGyro)));
            IMUDataYGyro=-[RawData.M1HIGHRESIMUygyro];IMUDataYGyro=IMUDataYGyro(:,all(~isnan(IMUDataYGyro),length(IMUDataYGyro)));
            IMUDataZGyro=-[RawData.M1HIGHRESIMUzgyro];IMUDataZGyro=IMUDataZGyro(:,all(~isnan(IMUDataZGyro),length(IMUDataZGyro)));
            IMUDataXMag=[RawData.M1HIGHRESIMUxmag];IMUDataXMag=IMUDataXMag(:,all(~isnan(IMUDataXMag),length(IMUDataXMag)));
            IMUDataYMag=-[RawData.M1HIGHRESIMUymag];IMUDataYMag=IMUDataYMag(:,all(~isnan(IMUDataYMag),length(IMUDataYMag)));
            IMUDataZMag=-[RawData.M1HIGHRESIMUzmag];IMUDataZMag=IMUDataZMag(:,all(~isnan(IMUDataZMag),length(IMUDataZMag)));
        elseif strcmp(OutCoordinate,'ENU')%iPhone
            IMUDataXAcc=[RawData.M1HIGHRESIMUyacc];IMUDataXAcc=IMUDataXAcc(:,all(~isnan(IMUDataXAcc),length(IMUDataXAcc)));
            IMUDataYAcc=[RawData.M1HIGHRESIMUxacc];IMUDataYAcc=IMUDataYAcc(:,all(~isnan(IMUDataYAcc),length(IMUDataYAcc)));
            IMUDataZAcc=-[RawData.M1HIGHRESIMUzacc];IMUDataZAcc=IMUDataZAcc(:,all(~isnan(IMUDataZAcc),length(IMUDataZAcc)));
            IMUDataXGyro=[RawData.M1HIGHRESIMUygyro];IMUDataXGyro=IMUDataXGyro(:,all(~isnan(IMUDataXGyro),length(IMUDataXGyro)));
            IMUDataYGyro=[RawData.M1HIGHRESIMUxgyro];IMUDataYGyro=IMUDataYGyro(:,all(~isnan(IMUDataYGyro),length(IMUDataYGyro)));
            IMUDataZGyro=-[RawData.M1HIGHRESIMUzgyro];IMUDataZGyro=IMUDataZGyro(:,all(~isnan(IMUDataZGyro),length(IMUDataZGyro)));
            IMUDataXMag=[RawData.M1HIGHRESIMUymag];IMUDataXMag=IMUDataXMag(:,all(~isnan(IMUDataXMag),length(IMUDataXMag)));
            IMUDataYMag=[RawData.M1HIGHRESIMUxmag];IMUDataYMag=IMUDataYMag(:,all(~isnan(IMUDataYMag),length(IMUDataYMag)));
            IMUDataZMag=-[RawData.M1HIGHRESIMUzmag];IMUDataZMag=IMUDataZMag(:,all(~isnan(IMUDataZMag),length(IMUDataZMag)));
        elseif strcmp(OutCoordinate,'NEU')||strcmp(OutCoordinate,'END')||strcmp(OutCoordinate,'WSD')
            %% Todo
            error('OutCoordinate type to be supportted!');            
        else error('OutCoordinate type not supportted!');
        end
        %% From the data into structure
        IMUDataArray=[IMUTimeArray',IMUDtArray',...
            IMUDataXAcc',IMUDataYAcc',IMUDataZAcc',...
            IMUDataXGyro',IMUDataYGyro',IMUDataZGyro',...
            IMUDataXMag',IMUDataYMag',IMUDataZMag'];
        if strcmp(SensorType,'IMU6')||strcmp(SensorType,'IMU6Only')
            IMUDataArray=IMUDataArray(:,1:8);
            IMUData=cell2struct(num2cell(IMUDataArray), {'Time','dt',...
                'accelX','accelY','accelZ',...
                'omegaX','omegaY','omegaZ'}, 2);
            imum = cellfun(@(x) x', num2cell([...
                [IMUData.accelX]' [IMUData.accelY]' [IMUData.accelZ]' ...
                [IMUData.omegaX]' [IMUData.omegaY]' [IMUData.omegaZ]' ...
                ], 2), 'UniformOutput', false);
            [IMUData.acc_omega] = deal(imum{:});
        elseif strcmp(SensorType,'IMU9')||strcmp(SensorType,'IMU9Only')
            IMUData=cell2struct(num2cell(IMUDataArray), {'Time','dt',...
                'accelX','accelY','accelZ',...
                'omegaX','omegaY','omegaZ',...
                'headX','headY','headZ'}, 2);
            imum = cellfun(@(x) x', num2cell([...
                [IMUData.accelX]' [IMUData.accelY]' [IMUData.accelZ]' ...
                [IMUData.omegaX]' [IMUData.omegaY]' [IMUData.omegaZ]' ...
                [IMUData.headX]' [IMUData.headY]' [IMUData.headZ]' ...
                ], 2), 'UniformOutput', false);
            [IMUData.acc_omega] = deal(imum{:});
        else error('Sensor type error!');
        end
        output_args=IMUData;
    %% Porcess the GPS data from raw array
    elseif strcmp(SensorType,'GPS')
        GPSDataLon=[RawData.M1GPSRAWINTlon];GPSDataLon=GPSDataLon(:,all(~isnan(GPSDataLon),length(GPSDataLon)))./10000000;
        GPSDataLat=[RawData.M1GPSRAWINTlat];GPSDataLat=GPSDataLat(:,all(~isnan(GPSDataLat),length(GPSDataLat)))./10000000;
        GPSDataAlt=[RawData.M1GPSRAWINTalt];GPSDataAlt=GPSDataAlt(:,all(~isnan(GPSDataAlt),length(GPSDataAlt)))./1000;
        % Congvert from ell to xyz (ENU)
        AvrAlt=7.8;%(m)%local alt
        [rm,~,rl]=ellradii(mean(GPSDataLat));%get radii of earth
        GPSDataX=deg2rad(GPSDataLon).*(rl+AvrAlt);
        GPSDataY=deg2rad(GPSDataLat).*(rm+AvrAlt);
        GPSDataZ=GPSDataAlt-AvrAlt;
        % GPS Coordinate transform
        if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'NED')%PX4 QGC
            t=GPSDataX;
            GPSDataX=GPSDataY;
            GPSDataY=t;
            GPSDataZ=-GPSDataZ;
        elseif strcmp(OutCoordinate,'NWU')% Demo(maybe)
            t=GPSDataX;
            GPSDataX=GPSDataY;
            GPSDataY=-t;
        elseif strcmp(OutCoordinate,'ENU')
            % Do nothing.
        elseif strcmp(OutCoordinate,'NEU')||strcmp(OutCoordinate,'END')||strcmp(OutCoordinate,'WSD')
            %% Todo
            error('OutCoordinate type to be supportted!');            
        else error('OutCoordinate type not supportted!');
        end
        % Centrilize the corrodinate.
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);
        %% Output GPS data
        GPSDataArray=[GPSTimeArray',GPSDataX',GPSDataY',GPSDataZ'];
        GPSData=cell2struct(num2cell(GPSDataArray), {'Time','X','Y','Z'}, 2);

        for i = 1:numel(GPSData)
            GPSData(i).Position = gtsam.Point3(GPSData(i).X, GPSData(i).Y, GPSData(i).Z);
        end

        output_args=GPSData; 
    else error('Sensor type not supported!');
    end
%% Read data file from iPhone with SensorLog, Raw data    
elseif strcmp(DatasetType,'iPhone_SL')
    %% Import data from text file.
    % Codes Genarated by MATLAB for reading the CSV file
    % Auto-generated by MATLAB 

    %% Initialize variables.
    delimiter = ',';

    %% Read columns of data as strings:
    % For more information, see the TEXTSCAN documentation.
    formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

    %% Open the text file.
    fileID = fopen(Datafile,'r');

    %% Read columns of data according to format string.
    % This call is based on the structure of the file used to generate this
    % code. If an error occurs for a different file, try regenerating the code
    % from the Import Tool.
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);

    %% Close the text file.
    fclose(fileID);

    %% Convert the contents of columns containing numeric strings to numbers.
    % Replace non-numeric strings with NaN.
    raw = repmat({NaN},length(dataArray{1}),length(dataArray)-1);
    for col=1:length(dataArray)-1
        raw(1:length(dataArray{col}),col) = dataArray{col};
    end
    numericData = NaN(size(dataArray{1},1),size(dataArray,2));

    for col=[2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,40,41,42,43,44,45,46,47,48,49,50,51,53,67,68,69,70]
        % Converts strings in the input cell array to numbers. Replaced non-numeric
        % strings with NaN.
        rawData = dataArray{col};
        for row=2:size(rawData, 1);
            % Create a regular expression to detect and remove non-numeric prefixes and
            % suffixes.
            regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
            try
                result = regexp(rawData{row}, regexstr, 'names');
                numbers = result.numbers;

                % Detected commas in non-thousand locations.
                invalidThousandsSeparator = false;
                if any(numbers==',');
                    thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                    if isempty(regexp(thousandsRegExp, ',', 'once'));
                        numbers = NaN;
                        invalidThousandsSeparator = true;
                    end
                end
                % Convert numeric strings to numbers.
                if ~invalidThousandsSeparator;
                    numbers = textscan(strrep(numbers, ',', ''), '%f64');
                    numericData(row, col) = numbers{1};
                    raw{row, col} = numbers{1};
                end
            catch me
            end
        end
    end
    %% Split data into numeric and cell columns.
     %rawNumericColumns = raw(2:end, [2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,40,41,42,43,44,45,46,47,48,49,50,51,53,67,68,69,70]);
     %rawCellColumns = raw(2:end, [1,3,4,39,52,54,55,56,57,58,59,60,61,62,63,64,65,66]);
    %% Replace non-numeric cells with NaN 
     R = cellfun(@(x) strcmp(x,''),raw); % Find non-numeric cells
     raw(R) = {NaN}; % Replace non-numeric cells
    %% Create output variable
    RawData = cell2struct(raw(2:end,:),raw(1,:),2);
    %% Clear temporary variables
    clearvars filename delimiter formatSpec fileID dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp me rawNumericColumns rawCellColumns R;
    %% Porcess the IMU data from raw array
    if strcmp(SensorType,'IMU6')||strcmp(SensorType,'IMU9')||strcmp(SensorType,'IMU6Only')||strcmp(SensorType,'IMU9Only')
        IMUData=[];
        IMUTime_P=0;
        IMUTimeFirst=RawData(1).accelerometerTimestamp_sinceReboot;
        if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'NED')%PX4 QGC
            for RawDataIndex=1:length(RawData)
                if ~isnan(RawData(RawDataIndex).gyroRotationX) && ...
                        ~isnan(RawData(RawDataIndex).accelerometerAccelerationX)&&...
                        ~isnan(RawData(RawDataIndex).locationHeadingX)
                    IMUTime = RawData(RawDataIndex).accelerometerTimestamp_sinceReboot;
                    IMUTime=IMUTime-IMUTimeFirst;
                    IMUDt = IMUTime-IMUTime_P;
                   
                    IMUData=[IMUData;IMUTime,IMUDt,...
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationY,...%accx
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationX,...%accy
                        9.81*RawData(RawDataIndex).accelerometerAccelerationZ,...%accz
                        RawData(RawDataIndex).gyroRotationY,...%gyrox
                        RawData(RawDataIndex).gyroRotationX,...%gyroy
                        RawData(RawDataIndex).gyroRotationZ,...%gyroz
                        RawData(RawDataIndex).locationHeadingY,...%headx
                        RawData(RawDataIndex).locationHeadingX,...%headx
                        RawData(RawDataIndex).locationHeadingZ,...%headx
                        ];

                   IMUTime_P= IMUTime; 
                end
            end
        elseif strcmp(OutCoordinate,'NWU')%Demo(maybe)
            for RawDataIndex=1:length(RawData)
                if ~isnan(RawData(RawDataIndex).gyroRotationX) && ...
                        ~isnan(RawData(RawDataIndex).accelerometerAccelerationX)&&...
                        ~isnan(RawData(RawDataIndex).locationHeadingX)
                    IMUTime = RawData(RawDataIndex).accelerometerTimestamp_sinceReboot;
                    IMUTime=IMUTime-IMUTimeFirst;
                    IMUDt = IMUTime-IMUTime_P;
                   
                    IMUData=[IMUData;IMUTime,IMUDt,...
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationY,...%accx
                        9.81*RawData(RawDataIndex).accelerometerAccelerationX,...%accy
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationZ,...%accz
                        RawData(RawDataIndex).gyroRotationY,...%gyrox
                        -RawData(RawDataIndex).gyroRotationX,...%gyroy
                        RawData(RawDataIndex).gyroRotationZ,...%gyroz
                        -RawData(RawDataIndex).locationHeadingY,...%headx
                        RawData(RawDataIndex).locationHeadingX,...%headx
                        -RawData(RawDataIndex).locationHeadingZ,...%headx
                        ];

                   IMUTime_P= IMUTime; 
                end
            end
        elseif strcmp(OutCoordinate,'ENU') 
            for RawDataIndex=1:length(RawData)
                if ~isnan(RawData(RawDataIndex).gyroRotationX) && ...
                        ~isnan(RawData(RawDataIndex).accelerometerAccelerationX)&&...
                        ~isnan(RawData(RawDataIndex).locationHeadingX)
                    IMUTime = RawData(RawDataIndex).accelerometerTimestamp_sinceReboot;
                    IMUTime=IMUTime-IMUTimeFirst;
                    IMUDt = IMUTime-IMUTime_P;
                    
                    IMUData=[IMUData;IMUTime,IMUDt,...
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationX,...%accx
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationY,...%accy
                        -9.81*RawData(RawDataIndex).accelerometerAccelerationZ,...%accz
                        RawData(RawDataIndex).gyroRotationX,...%gyrox
                        RawData(RawDataIndex).gyroRotationY,...%gyroy
                        RawData(RawDataIndex).gyroRotationZ,...%gyroz
                        -RawData(RawDataIndex).locationHeadingY,...%headx
                        -RawData(RawDataIndex).locationHeadingX,...%headx
                        -RawData(RawDataIndex).locationHeadingZ,...%headx
                        ];

                   IMUTime_P= IMUTime; 
                end
            end
        elseif strcmp(OutCoordinate,'NEU')||strcmp(OutCoordinate,'END')
            %% Todo
            error('OutCoordinate type to be supportted!');            
        else error('OutCoordinate type not supportted!');
        end
        if strcmp(SensorType,'IMU6')||strcmp(SensorType,'IMU6Only')
            IMUData=IMUData(:,1:8);
            IMUData=cell2struct(num2cell(IMUData), {'Time','dt','accelX','accelY','accelZ','omegaX','omegaY','omegaZ'}, 2);
            imum = cellfun(@(x) x', num2cell([ [IMUData.accelX]' [IMUData.accelY]' [IMUData.accelZ]' [IMUData.omegaX]' [IMUData.omegaY]' [IMUData.omegaZ]' ], 2), 'UniformOutput', false);
            [IMUData.acc_omega] = deal(imum{:});          
        elseif strcmp(SensorType,'IMU9')||strcmp(SensorType,'IMU9Only')
            IMUData=cell2struct(num2cell(IMUData), {'Time','dt',...
                'accelX','accelY','accelZ',...
                'omegaX','omegaY','omegaZ',...
                'headX','headY','headZ'}, 2);
            imum = cellfun(@(x) x', num2cell([ [IMUData.accelX]' [IMUData.accelY]' [IMUData.accelZ]' ...
                [IMUData.omegaX]' [IMUData.omegaY]' [IMUData.omegaZ]' ...
                [IMUData.headX]' [IMUData.headY]' [IMUData.headZ]'], 2), 'UniformOutput', false);
            [IMUData.acc_omega] = deal(imum{:});   
        end
        clear imum
        output_args=IMUData;
    %% Porcess the GPS data from raw array
    elseif strcmp(SensorType,'GPS')
                % read raw data
        GPSLongitude=[RawData.locationLongitude];
        GPSLatitude=[RawData.locationLatitude];
        GPSAltitude=[RawData.locationAltitude];
        %GPSTimeI=[RawData.locationAltitude]-[RawData.locationAltitude];
        GPSTime=[RawData.accelerometerTimestamp_sinceReboot];
        % chop nan
        GPSTime=GPSTime(:,all(~isnan(GPSLatitude),length(GPSLatitude)));
        GPSLongitude=GPSLongitude(:,all(~isnan(GPSLongitude),length(GPSLongitude)));
        GPSLatitude=GPSLatitude(:,all(~isnan(GPSLatitude),length(GPSLatitude)));
        GPSAltitude=GPSAltitude(:,all(~isnan(GPSAltitude),length(GPSAltitude)));
        
        % convert from ell to xyz(ENU)rom
        GPSLatMean=mean(GPSLatitude);
        AvrAlt=7.8;%(m)%local alt
        [rm,~,rl]=ellradii(GPSLatMean);%get radii of earth
        
        GPSDataX=deg2rad(GPSLongitude).*(rl+AvrAlt);
        GPSDataY=deg2rad(GPSLatitude).*(rm+AvrAlt);
        GPSDataZ=GPSAltitude-AvrAlt;
        
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);
        GPSTime=GPSTime-GPSTime(1);
        GPSData=[GPSTime', GPSDataX', GPSDataY', GPSDataZ'];


        
            % GPS Coordinate transform
            if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'NED')%PX4 QGC
                t=GPSData(:,3);
                GPSData(:,3)=GPSData(:,4);
                GPSData(:,4)=t;
                GPSData(:,5)=-GPSData(:,5);
            elseif strcmp(OutCoordinate,'NWU')% Demo(maybe)
                t=GPSData(:,3);
                GPSData(:,3)=GPSData(:,4);
                GPSData(:,4)=-t;
            elseif strcmp(OutCoordinate,'ENU')%iPhone
                % Do nothing.
            elseif strcmp(OutCoordinate,'NEU')||strcmp(OutCoordinate,'END')||strcmp(OutCoordinate,'WSD')
                %% Todo
                error('OutCoordinate type to be supportted!');            
            else error('OutCoordinate type not supportted!');
            end
            
            GPSData=cell2struct(num2cell(GPSData), {'Time','X','Y','Z'}, 2);
            for i = 1:numel(GPSData)
                GPSData(i).Position = gtsam.Point3(GPSData(i).X, GPSData(i).Y, GPSData(i).Z);
            end
            output_args=GPSData;
    end
%% Read data file from iPhone with SensorLog, Cooked data    
elseif strcmp(DatasetType,'iPhone_SL_Opt')
    %% Import data from text file.
    % Codes Genarated by MATLAB for reading the CSV file
    % Auto-generated by MATLAB 

    %% Initialize variables.
    delimiter = ',';

    %% Read columns of data as strings:
    % For more information, see the TEXTSCAN documentation.
    formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

    %% Open the text file.
    fileID = fopen(Datafile,'r');

    %% Read columns of data according to format string.
    % This call is based on the structure of the file used to generate this
    % code. If an error occurs for a different file, try regenerating the code
    % from the Import Tool.
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);

    %% Close the text file.
    fclose(fileID);

    %% Convert the contents of columns containing numeric strings to numbers.
    % Replace non-numeric strings with NaN.
    raw = repmat({NaN},length(dataArray{1}),length(dataArray)-1);
    for col=1:length(dataArray)-1
        raw(1:length(dataArray{col}),col) = dataArray{col};
    end
    numericData = NaN(size(dataArray{1},1),size(dataArray,2));

    for col=[2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,40,41,42,43,44,45,46,47,48,49,50,51,53,67,68,69,70]
        % Converts strings in the input cell array to numbers. Replaced non-numeric
        % strings with NaN.
        rawData = dataArray{col};
        for row=2:size(rawData, 1);
            % Create a regular expression to detect and remove non-numeric prefixes and
            % suffixes.
            regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
            try
                result = regexp(rawData{row}, regexstr, 'names');
                numbers = result.numbers;

                % Detected commas in non-thousand locations.
                invalidThousandsSeparator = false;
                if any(numbers==',');
                    thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                    if isempty(regexp(thousandsRegExp, ',', 'once'));
                        numbers = NaN;
                        invalidThousandsSeparator = true;
                    end
                end
                % Convert numeric strings to numbers.
                if ~invalidThousandsSeparator;
                    numbers = textscan(strrep(numbers, ',', ''), '%f64');
                    numericData(row, col) = numbers{1};
                    raw{row, col} = numbers{1};
                end
            catch me
            end
        end
    end
    %% Split data into numeric and cell columns.
     %rawNumericColumns = raw(2:end, [2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,40,41,42,43,44,45,46,47,48,49,50,51,53,67,68,69,70]);
     %rawCellColumns = raw(2:end, [1,3,4,39,52,54,55,56,57,58,59,60,61,62,63,64,65,66]);
    %% Replace non-numeric cells with NaN 
     R = cellfun(@(x) strcmp(x,''),raw); % Find non-numeric cells
     raw(R) = {NaN}; % Replace non-numeric cells
    %% Create output variable
    RawData = cell2struct(raw(2:end,:),raw(1,:),2);
    %% Clear temporary variables
    clearvars filename delimiter formatSpec fileID dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp me rawNumericColumns rawCellColumns R;
    sprintf('Read file done');
    %% Set up Time array
    if (~strcmp(SensorType,'IMUOnly'))&&(~strcmp(SensorType,'IMU6Only'))&&(~strcmp(SensorType,'IMU9Only'))
        GPSTimeArray=[RawData.accelerometerTimestamp_sinceReboot];
        FirstGPSTimeIndex=find(sum(~isnan([RawData.accelerometerTimestamp_sinceReboot]),1) > 0, 1 ,'first');
        GPSTimeArray=GPSTimeArray(:,all(~isnan(GPSTimeArray),length(GPSTimeArray)));
    end
    IMUTimeArray=[RawData.accelerometerTimestamp_sinceReboot];
    if (~strcmp(SensorType,'IMUOnly'))&&(~strcmp(SensorType,'IMU6Only'))&&(~strcmp(SensorType,'IMU9Only'))
        FirstIMUTime = GPSTimeArray(1)-IMUTimeArray(FirstGPSTimeIndex);
        IMUTimeArray=FirstIMUTime+IMUTimeArray;
    end
    ChopIndex=[RawData.motionUserAccelerationX];
    tIMUDataArray=[RawData.accelerometerTimestamp_sinceReboot];
    IMUTimeArray=IMUTimeArray(:,all(~isnan(ChopIndex),length(ChopIndex)));
    IMUTimeArray=IMUTimeArray-IMUTimeArray(1);
    IMUDtArray=IMUTimeArray;
    for i=1:length(IMUTimeArray)-1
        IMUDtArray(i)=IMUTimeArray(i+1)-IMUTimeArray(i);
    end
    IMUDtArray(length(IMUDtArray))=mean(IMUDtArray(1:length(IMUDtArray)-1));
    if strcmp(SensorType,'IMU')
        if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'ENU')%iPhone
            IMUDataXAcc=-9.81.*[RawData.motionUserAccelerationX];IMUDataXAcc=IMUDataXAcc(:,all(~isnan(IMUDataXAcc),length(IMUDataXAcc)));
            IMUDataYAcc=-9.81.*[RawData.motionUserAccelerationY];IMUDataYAcc=IMUDataYAcc(:,all(~isnan(IMUDataYAcc),length(IMUDataYAcc)));
            IMUDataZAcc=-9.81.*[RawData.motionUserAccelerationZ];IMUDataZAcc=IMUDataZAcc(:,all(~isnan(IMUDataZAcc),length(IMUDataZAcc)));
            IMUDataXGyro=[RawData.motionRotationRateX];IMUDataXGyro=IMUDataXGyro(:,all(~isnan(IMUDataXGyro),length(IMUDataXGyro)));
            IMUDataYGyro=[RawData.motionRotationRateY];IMUDataYGyro=IMUDataYGyro(:,all(~isnan(IMUDataYGyro),length(IMUDataYGyro)));
            IMUDataZGyro=[RawData.motionRotationRateZ];IMUDataZGyro=IMUDataZGyro(:,all(~isnan(IMUDataZGyro),length(IMUDataZGyro)));
            IMUDataYaw=[RawData.motionYaw];IMUDataYaw=IMUDataYaw(:,all(~isnan(IMUDataYaw),length(IMUDataYaw)));
            IMUDataRoll=[RawData.motionRoll];IMUDataRoll=IMUDataRoll(:,all(~isnan(IMUDataRoll),length(IMUDataRoll)));
            IMUDataPitch=[RawData.motionPitch];IMUDataPitch=IMUDataPitch(:,all(~isnan(IMUDataPitch),length(IMUDataPitch)));
            IMUDataW=[RawData.motionQuaternionW];IMUDataW=IMUDataW(:,all(~isnan(IMUDataW),length(IMUDataW)));
            IMUDataX=[RawData.motionQuaternionX];IMUDataX=IMUDataX(:,all(~isnan(IMUDataX),length(IMUDataX)));
            IMUDataY=[RawData.motionQuaternionY];IMUDataY=IMUDataY(:,all(~isnan(IMUDataY),length(IMUDataY)));
            IMUDataZ=[RawData.motionQuaternionZ];IMUDataZ=IMUDataZ(:,all(~isnan(IMUDataZ),length(IMUDataZ)));
        else error('OutCoordinate not right');
        end
        IMUDataArray=[IMUTimeArray',IMUDtArray',...
            IMUDataXAcc',IMUDataYAcc',IMUDataZAcc',...
            IMUDataXGyro',IMUDataYGyro',IMUDataZGyro',...
            IMUDataYaw',IMUDataRoll',IMUDataPitch',...
            IMUDataW',IMUDataX',IMUDataY',IMUDataZ'];

            IMUData=cell2struct(num2cell(IMUDataArray), {'Time','dt',...
                'accelX','accelY','accelZ',...
                'omegaX','omegaY','omegaZ',...
                'Yaw','Roll','Pitch',...
                'Qw','Qx','Qy','Qz'}, 2);
            imum = cellfun(@(x) x', num2cell([...
                [IMUData.Qw]' [IMUData.Qx]' [IMUData.Qy]' [IMUData.Qz]' ...
                ], 2), 'UniformOutput', false);
            [IMUData.Quaternion] = deal(imum{:});
            
            imum = cellfun(@(x) x', num2cell([...
                [IMUData.accelX]' [IMUData.accelY]' [IMUData.accelZ]' ...
                [IMUData.omegaX]' [IMUData.omegaY]' [IMUData.omegaZ]' ...
                ], 2), 'UniformOutput', false);
            [IMUData.acc_omega] = deal(imum{:});
            
        output_args = IMUData;
    elseif strcmp(SensorType,'GPS')
        

        
        
        % read raw data
        GPSLongitude=[RawData.locationLongitude];
        GPSLatitude=[RawData.locationLatitude];
        GPSAltitude=[RawData.locationAltitude];
        %GPSTimeI=[RawData.locationAltitude]-[RawData.locationAltitude];
        GPSTime=[RawData.accelerometerTimestamp_sinceReboot];
        % chop nan
        GPSTime=GPSTime(:,all(~isnan(GPSLatitude),length(GPSLatitude)));
        GPSLongitude=GPSLongitude(:,all(~isnan(GPSLongitude),length(GPSLongitude)));
        GPSLatitude=GPSLatitude(:,all(~isnan(GPSLatitude),length(GPSLatitude)));
        GPSAltitude=GPSAltitude(:,all(~isnan(GPSAltitude),length(GPSAltitude)));
        
        % convert from ell to xyz(ENU)rom
        GPSLatMean=mean(GPSLatitude);
        AvrAlt=7.8;%(m)%local alt
        [rm,~,rl]=ellradii(GPSLatMean);%get radii of earth
        
        GPSDataX=deg2rad(GPSLongitude).*(rl+AvrAlt);
        GPSDataY=deg2rad(GPSLatitude).*(rm+AvrAlt);
        GPSDataZ=GPSAltitude-AvrAlt;
        
        GPSDataX=GPSDataX-GPSDataX(1);
        GPSDataY=GPSDataY-GPSDataY(1);
        GPSDataZ=GPSDataZ-GPSDataZ(1);
        GPSTime=GPSTime-GPSTime(1);
        
        GPSData=[GPSTime', GPSDataX', GPSDataY', GPSDataZ'];

            % GPS Coordinate transform
            if strcmp(OutCoordinate,'Raw')||strcmp(OutCoordinate,'NED')%PX4 QGC
                t=GPSData(:,3);
                GPSData(:,3)=GPSData(:,4);
                GPSData(:,4)=t;
                GPSData(:,5)=-GPSData(:,5);
            elseif strcmp(OutCoordinate,'NWU')% Demo(maybe)
                t=GPSData(:,3);
                GPSData(:,3)=GPSData(:,4);
                GPSData(:,4)=-t;
            elseif strcmp(OutCoordinate,'ENU')%iPhone
                % Do nothing.
            elseif strcmp(OutCoordinate,'NEU')||strcmp(OutCoordinate,'END')||strcmp(OutCoordinate,'WSD')
                %% Todo
                error('OutCoordinate type to be supportted!');            
            else error('OutCoordinate type not supportted!');
            end
            
            GPSData=cell2struct(num2cell(GPSData), {'Time','X','Y','Z'}, 2);
%             for i = 1:numel(GPSData)
%                 GPSData(i).Position = gtsam.Point3(GPSData(i).X, GPSData(i).Y, GPSData(i).Z);
%             end
            output_args=GPSData;
    end
elseif strcmp(DatasetType,'GTSAM_Demo')
    if strcmp(SensorType,'IMU6')||strcmp(SensorType,'IMU6Only')
        %IMU Data
        IMU_data = importdata(Datafile);
        IMU_data = cell2struct(num2cell(IMU_data.data), IMU_data.colheaders, 2);
        imum = cellfun(@(x) x', num2cell([ [IMU_data.accelX]' [IMU_data.accelY]' [IMU_data.accelZ]' [IMU_data.omegaX]' [IMU_data.omegaY]' [IMU_data.omegaZ]' ], 2), 'UniformOutput', false);
        [IMU_data.acc_omega] = deal(imum{:});
        clear imum
        output_args=IMU_data;
    elseif strcmp(SensorType,'GPS')
        %GPS data
        GPS_data = importdata(Datafile);
        GPS_data = cell2struct(num2cell(GPS_data.data), GPS_data.colheaders, 2);
%         for i = 1:numel(GPS_data)
%            GPS_data(i).Position = gtsam.Point3(GPS_data(i).X, GPS_data(i).Y, GPS_data(i).Z);
%         end
        output_args=GPS_data; 
    else
        error('ERROR: Sensor type is not valied in Demo mode!');
    end
else error('Dataset type not supported');
end
end

