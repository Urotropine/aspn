function [ output_args ] = ReadRosData( Datafile )
%READROSDATA Summary of this function goes here
%   Detailed explanation goes here
import gtsam.*;
RawData = importdata(Datafile);
if isa(RawData,'struct')
    RawData = cell2struct(num2cell(RawData.data), RawData.colheaders, 2);
else
    RawData = NaN;
end
output_args = RawData;
end

