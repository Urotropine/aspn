%% Raw data reading and pre-process section
% Read data from file
if NeediSam2
    import gtsam.*;
end
disp('-->Begin read data form file.');
    FileIMUAtmData = ReadRosData('IMUAtmData.txt');
    FileIMUDataData = ReadRosData('IMUDataData.txt');
    FileIMUDataRawData = ReadRosData('IMUDataRawData.txt');
    FileIMUMagData = ReadRosData('IMUMagData.txt');
    FileIMUTempData = ReadRosData('IMUTempData.txt');
    FileLocalPostionLocalData = ReadRosData('LocalPostionLocalData.txt');
    FileLocalPostionPoseData = ReadRosData('LocalPostionPoseData.txt');
    FileLocalPostionVelocityData = ReadRosData('LocalPostionVelocityData.txt');
    FilePositionGlobaGlobalData = ReadRosData('PositionGlobaGlobalData.txt');
    FilePositionGlobalCompassData = ReadRosData('PositionGlobalCompassData.txt');
    FilePositionGlobalGpVelData = ReadRosData('PositionGlobalGpVelData.txt');
    FilePositionGlobalLocalData = ReadRosData('PositionGlobalLocalData.txt');
    FilePositionGlobalRawFixData = ReadRosData('PositionGlobalRawFixData.txt');
    FilePositionGlobalRawGPSVelData = ReadRosData('PositionGlobalRawGPSVelData.txt');
    FilePositionGlobalRelAltData = ReadRosData('PositionGlobalRelAltData.txt');
    FilePx4flowOpticalFlowRadData = ReadRosData('Px4flowOpticalFlowRadData.txt');
disp('<--Reading data done.');

% Pre-process of data
disp('-->Processing raw structure.')
% Baro data
disp('----> ATM data')
    fn = fieldnames(FileIMUAtmData);
    t = [FileIMUAtmData.IMUAtmData]';
    t_index = find(t>200);
    % Baro data type (11)
    t = num2cell(ones(length(t_index),1)*11);
    [BaroData(1:length(t_index)).Type]=deal(t{:});
    % BaroData Time
    t=[FileIMUAtmData.IMUAtmTime]';
    t=num2cell((t(t_index)-FileIMUAtmData(1).IMUAtmTime)*(1e-9));
    [BaroData.Time]=deal(t{:});
    % BaroData Data
    t = [FileIMUAtmData.IMUAtmData]';
    t = t(t_index);
    t = atmospalt(t);
    t = t-t(1);
    t=num2cell(t);
    [BaroData.Alt]=deal(t{:});
% IMU data
disp('----> IMU data')
    fn = fieldnames(FileIMUDataData);
    % IMU data type (1)
    t = num2cell(ones(length(FileIMUDataData),1));
    [IMUData(1:length(FileIMUDataData)).Type]=deal(t{:});
    % IMUData Time
    t=num2cell(([FileIMUDataData.IMUDataTime]'-FileIMUDataData(1).IMUDataTime)*(1e-9));
    [IMUData.Time]=deal(t{:});
    % IMUData Dt
    p=[IMUData.Time];q=[0,p];
    t=num2cell(p'-q(1:length(q)-1)');
    [IMUData.Dt]=deal(t{:});
    % Frame Trans:
    % Mavros IMU data is under x:forward,y:left,z:up (NWU-body), where Q is
    % will trans IMU data form NWU-body to ENU-world
    % so here we will do noting with the IMU data, just remember the acc &
    % gyro is under NWU-body and the world-acc & gyro is ENU.
        
    % IMU Orign
    t=[[FileIMUDataData.IMUDataOrienQw]' [FileIMUDataData.IMUDataOrienQx]'...
        [FileIMUDataData.IMUDataOrienQy]' [FileIMUDataData.IMUDataOrienQz]' ];
    t = quatinv(t);
    
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Q]=deal(t{:});
     % IMU Orign Cov
    t=[FileIMUDataData.IMUDataOrienCovElems0]';
    for i=[8:15]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_Q]=deal(t{:});
    % IMU acc   
    t=num2cell([FileIMUDataData.IMUDataLinearAccX]');
    [IMUData.AccX]=deal(t{:});  
    t=num2cell([FileIMUDataData.IMUDataLinearAccY]');
    [IMUData.AccY]=deal(t{:});
    t=num2cell([FileIMUDataData.IMUDataLinearAccZ]');
    [IMUData.AccZ]=deal(t{:}); 
    %IMU acc Cov
    t=[FileIMUDataData.IMUDataLinearAccCovElems0]';
    for i=[32:39]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_acc]=deal(t{:});  
    % IMU Omega   
    t=num2cell([FileIMUDataData.IMUDataAngVelX]');
    [IMUData.OmegaX]=deal(t{:});
    t=num2cell([FileIMUDataData.IMUDataAngVelY]');
    [IMUData.OmegaY]=deal(t{:});
    t=num2cell([FileIMUDataData.IMUDataAngVelZ]');
    [IMUData.OmegaZ]=deal(t{:}); 
    %IMU Omega Cov
    t=[FileIMUDataData.IMUDataAngVelCovElems0]';
    for i=[20:27]
        t= [t [FileIMUDataData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [IMUData.Cov_omega]=deal(t{:});      
    % IMU acc-omega (required by isam) %NOTICE: FRAME TRANS HERE !!!!
%     t = cellfun(@(x) x', num2cell([...
%         -[FileIMUDataData.IMUDataLinearAccY]' [FileIMUDataData.IMUDataLinearAccX]' [FileIMUDataData.IMUDataLinearAccZ]' ...
%         -[FileIMUDataData.IMUDataAngVelY]' [FileIMUDataData.IMUDataAngVelX]' [FileIMUDataData.IMUDataAngVelZ]'...
%         ],2), 'UniformOutput', false);
   %[IMUData.acc_omega] = deal(t{:});
   clear t p q;
% GPS data Here is ENU
disp('----> GPS data')
    fn = fieldnames(FilePositionGlobalRawFixData);
    % GPS data type (2)
    t = num2cell(2*ones(length(FilePositionGlobalRawFixData),1));
    [GPSData(1:length(FilePositionGlobalRawFixData)).Type]=deal(t{:});
    % GPSData Time
    t=num2cell(([FilePositionGlobalRawFixData.PositionGlobalRawFixTime]'-FilePositionGlobalRawFixData(1).PositionGlobalRawFixTime)*(1e-9));
    [GPSData.Time]=deal(t{:});
    % GPS status & service type
    t=num2cell([FilePositionGlobalRawFixData.PositionGlobalRawFixStatus]');
    [GPSData.Status]=deal(t{:});
    t=num2cell([FilePositionGlobalRawFixData.PositionGlobalRawFixServiceType]');
    [GPSData.ServiceType]=deal(t{:});
    % GPS location
    Lat = [FilePositionGlobalRawFixData.PositionGlobalRawFixLat];
    Lon = [FilePositionGlobalRawFixData.PositionGlobalRawFixLon]; 
    Alt = [FilePositionGlobalRawFixData.PositionGlobalRawFixAlt];
    AvrAlt=7.8;%(m)%local alt
    [rm,~,rl]=ellradii(mean(Lat));%get radii of earth
    GPSDataX=deg2rad(Lon).*(rl+AvrAlt);
    GPSDataY=deg2rad(Lat).*(rm+AvrAlt);
    GPSDataZ=Alt-AvrAlt;
    
    % Frame trnas
    
    % reset cood
    GPSDataX=GPSDataX-GPSDataX(1);
    GPSDataY=GPSDataY-GPSDataY(1);
    GPSDataZ=GPSDataZ-GPSDataZ(1);
    t=num2cell(GPSDataX');
    [GPSData.LocalX]=deal(t{:}); 
    t=num2cell(GPSDataY');
    [GPSData.LocalY]=deal(t{:}); 
    t=num2cell(GPSDataZ');
    [GPSData.LocalZ]=deal(t{:}); 
    % GPS Vel
    t=num2cell([FilePositionGlobalRawGPSVelData.PositionGlobalRawGPSVelTwistLinearX]');
    [GPSData.Vx]=deal(t{:});
    t=num2cell([FilePositionGlobalRawGPSVelData.PositionGlobalRawGPSVelTwistLinearY]');
    [GPSData.Vy]=deal(t{:});
    t=num2cell([FilePositionGlobalRawGPSVelData.PositionGlobalRawGPSVelTwistLinearZ]');
    [GPSData.Vz]=deal(t{:});
    % put-in the data

    
    
    % GPS position Cov
    t=[FilePositionGlobalRawFixData.PositionGlobalRawFixPositionCovElems0]';
    for i=[8:15]
        t= [t [FilePositionGlobalRawFixData.(fn{i})]'];
    end
    t = cellfun(@(x) x', num2cell(t,2), 'UniformOutput', false);
    [GPSData.Cov_pose]=deal(t{:}); 
    % GPS postion combined used by iSAM
    if NeediSam2
        for i = 1:numel(GPSData)
            GPSData(i).Position = gtsam.Point3(GPSData(i).LocalX, GPSData(i).LocalY, GPSData(i).LocalZ);
        end
    end
       clear fn p q Lat Lon Alt AvrAlt i rl rm t GPSDataX GPSDataY GPSDataZ;
% Optialflow data
disp('----> Opticalflow data')
    fn = fieldnames(FilePx4flowOpticalFlowRadData);
    % Optialflow data type (3)
    t = num2cell(3*ones(length(FilePx4flowOpticalFlowRadData),1));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).Type]=deal(t{:});
    % Optflow Time
    t=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadTime]'-FilePx4flowOpticalFlowRadData(1).Px4flowOpticalFlowRadTime)*(1e-9));
    [OptflowData.Time]=deal(t{:});
    % Optflow int time
    dt=num2cell(([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntTime_us]')*(1e-6));
    [OptflowData(1:length(FilePx4flowOpticalFlowRadData)).IntTime]=deal(dt{:});
    % Optflow pose
    dx = -[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntX];%NOTICE: FRAME TRANS HERE !!!!
    dy = -[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist].*[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntY];%NOTICE: FRAME TRANS HERE !!!!
    
    t = cellfun(@(x) x', num2cell([[FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntGyroX]' ...
        [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntGyroY]'...
        [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadIntGyroZ]' ...
        ],2), 'UniformOutput', false); %NOTICE: TODO FRAME TRANS HERE !!!!
    [OptflowData.Euler]=deal(t{:});
    % quality
   t=num2cell([FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadQlt]');
   [OptflowData.Quality]=deal(t{:});
    
    t_imu = [IMUData.Time];
    q_imu = [IMUData.Q];
    t=[OptflowData.Time];
    dt=[OptflowData.IntTime];
    q=zeros(4,length(OptflowData));
    j=1;
    for i=1:length(t_imu)
        if j<=length(OptflowData)
            if t(j)==t_imu(i)
                q(:,j)=q_imu(:,i);
                j=j+1;continue;
            elseif t(j)<t_imu(min(i+1,length(t_imu)))
                q(:,j)=(q_imu(:,i)+q_imu(:,i+1))/2;
                j=j+1;
                continue;
            else
                continue;
            end
        end
    end
    t=quatrotate((q'),[dx' dy' zeros(length(dx),1)]);
    dx_fix=t(:,1)';
    dy_fix=t(:,2)';
    
    X=zeros(1,length(dx));X(1)=dx_fix(1);
    Y=X;Y(1)=dy_fix(1);
    
    for i=2:length(OptflowData)
        X(i)=X(i-1)+dx_fix(i-1);
        Y(i)=Y(i-1)+dy_fix(i-1);
    end
    Vx = dx_fix./dt;Vy = dy_fix./dt;
    X=num2cell(X');Y=num2cell(Y');
    Dist = [FilePx4flowOpticalFlowRadData.Px4flowOpticalFlowRadDist];
    RadDist = num2cell(Dist');
    Z = num2cell(Dist'-Dist(1));
    Vx=num2cell(Vx');Vy=num2cell(Vy');
    
    [OptflowData.X]=deal(X{:});
    [OptflowData.Y]=deal(Y{:}); 
    [OptflowData.Z]=deal(Z{:}); 
    
    [OptflowData.Vx]=deal(Vx{:});
    [OptflowData.Vy]=deal(Vy{:}); 
    [OptflowData.Dist]=deal(RadDist{:}); 
 % matadata   
    disp('----> Reading sensor metadata')
    IMU_metadata = importdata('ExampleIMU_metadata');
    IMU_metadata = cell2struct(num2cell(IMU_metadata.data), IMU_metadata.colheaders, 2);
    if NeediSam2
        IMUinBody = Pose3.Expmap([IMU_metadata.BodyPtx; IMU_metadata.BodyPty; IMU_metadata.BodyPtz;
        IMU_metadata.BodyPrx; IMU_metadata.BodyPry; IMU_metadata.BodyPrz; ]);
        if ~IMUinBody.equals(Pose3, 1e-5)
          error 'Currently only support IMUinBody is identity, i.e. IMU and body frame are the same';
        end   
    end
 % Read data end
 disp('<-- Read data end')